﻿$(document).ready(function () {
    // initialisation des popovers
    $('.popovers').popover();

    //Condition de la modal rebuté dans edit Action et create action (si check alors modal de confirmation)
    $('input[type="checkbox"]').on('change', function (e) {
        if (e.target.checked) {
            $('#exampleModal').modal();
        } else {
            $('#exampleModal').modal('hide');
        }
    });
   

    $("#inputCloture").val("false")
    //Validation de la clôture de l'anc pour archivage (Action/create)
    $("#submitConfirmCloture").on("click",function () {
        $('#inputCloture').val("true");
        $('#Create').submit();
        console.log("submit clôture")
    })

    $('#inputClotureEdit').val("false");

    $("#submitConfirmClotureInEdit").on("click", function () {
        $('#inputClotureEdit').val("true");
        $('#Edit').submit();
        console.log("submit clôture true Edit")
    })



    //-------NonConformité Index au clic sur les états (_indexsommeNC) faire disparaitre les autres états----//

    $("#aTraiter").click(function () {

        $("input[type=search]").val("A traiter").keyup();

    })

    $("#enCours").click(function () {
        $("input[type=search]").val("En cours").keyup();
    })

    $("#fermer").click(function () {
        $("input[type=search]").val("Fermer").keyup();
    })

    $("#total").click(function () {
        $("input[type=search]").val("").keyup();
    })


    //Chargement tableau datatable pour liste NC
    $('#tab').DataTable(
        {
            language: {
                url: '../lib/Datatable/french.json',
            },
            responsive: true,
            ordering: false
        }
    );


    //Récupération des données Json de GetDataNCLine
    $.getJSON("/Home/GetDataNCLine", function (data) {
        $(function () {
            /* Initialisation des variables Month pour récupérer tous les mois
             * NCCount pour récupérer le nombre de NC par mois
             */
            var Month = [];
            var NCCount = [];

            /* Boucle pour parcourir toutes les données de data (recup Json)*/
            
            $(data).each(function (i, p) {
               // console.log(p)
                switch (p.name) {
                    case 1:
                        Month.push("Janvier");
                        break;
                    case 2:
                        Month.push("Février");
                        break;
                    case 3:
                        Month.push("Mars");
                        break;
                    case 4:
                        Month.push("Avril");
                        break;
                    case 5:
                        Month.push("Mai");
                        break;
                    case 6:
                        Month.push("Juin");
                        break;
                    case 7:
                        Month.push("Juillet");
                        break;
                    case 8:
                        Month.push("Août");
                        break;
                    case 9:
                        Month.push("Septembre");
                        break;
                    case 10:
                        Month.push("Octobre");
                        break;
                    case 11:
                        Month.push("Avril");
                        break;
                    default:
                        Month.push("Décembre");
                }
                //Le nombre de NC est pousser dans le tableau

                //console.log(Month)
                NCCount.push(p.count);
            })
            

            //-------------
            //- LINE CHART -
            //--------------
            var lineChartCanvas = $('#lineChart').get(0);
            var lineChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: true,
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: true,
                        }
                    }]
                }
            }

            var lineChartData = {
                //Variable Month appelé pour donner dans le graphique
                labels: Month,
                datasets: [
                    {
                        label: 'Nombre NC',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: true,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        //Variable NCCount appelé
                        data: NCCount
                    }
                ]
            }
            lineChartData.datasets[0].fill = false
            lineChartOptions.datasetFill = false

            var lineChart = new Chart(lineChartCanvas, {
                type: 'line',
                data: lineChartData,
                options: lineChartOptions
            })

        })
    })
                    //-------------
                    //- DONUT CHART -
                    //-------------
    $.getJSON("/Home/GetDataProduitNCDonut", function (data) {
        $(function () {

            var nameProduit = [];
            var QtProduit = [];

            $(data).each(function (index, value) {
                nameProduit.push(value.name);
                QtProduit.push(value.count);
            })

            var donutChartCanvas = $('#donutChart').get(0)
            var donutData = {
                labels: nameProduit,
                datasets: [
                    {
                        data: QtProduit,
                        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                    }
                ]
            }
            var donutOptions = {
                maintainAspectRatio: false,
                responsive: true,
            }

            new Chart(donutChartCanvas, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions
            })
        })
    })
            //-------------
            //- PIE CHART -
            //-------------
                $.getJSON("/Home/GetDataResponsabilitePie", function (data) {
                    $(function () {

                        var nameResponsabilite = [];
                        var countResponsabilite = [];

                        $(data).each(function (index, value) {
                            nameResponsabilite.push(value.name);
                            countResponsabilite.push(value.count);
                        })

                        
                        var pieChartCanvas = $('#pieChart').get(0)
                        var pieData = {
                            labels: nameResponsabilite,
                            datasets: [
                                {
                                    data: countResponsabilite,
                                    backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                                }
                            ]
                        }
                        var pieOptions = {
                            maintainAspectRatio: false,
                            responsive: true,
                        }

                        new Chart(pieChartCanvas, {
                            type: 'pie',
                            data: pieData,
                            options: pieOptions
                        })

                    })
                })

        })
