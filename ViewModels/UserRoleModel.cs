﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackQuality.Areas.Identity.Data;

namespace TrackQuality.ViewModels
{
    public class UserRoleModel
    {
    public ApplicationAuthUser applicationUser { get; set; }
    public List<SelectListItem> ApplicationRoles { get; set; }
    }
}
