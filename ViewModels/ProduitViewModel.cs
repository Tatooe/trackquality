﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackQuality.Models;

namespace TrackQuality.ViewModels
{
    public class ProduitViewModel
    {
        public Produit Produit { get; set; }
        public List<Produit> Produits { get; set; }
    }
}
