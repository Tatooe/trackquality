﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackQuality.Models;

namespace TrackQuality.ViewModels
{
    public class AnalyseActionByNCViewModel
    {
        public NonConformite NonConformite { get; set; }
        public AnalyseNC AnalyseNC { get; set; }
        public ActionNC ActionNC { get; set; }

        public List<NonConformite> NonConformites { get; set; }
        public List<AnalyseNC> AnalyseNCs { get; set; }
        public List<ActionNC> ActionNCs { get; set; }

    }
}
