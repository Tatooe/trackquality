﻿$(document).ready(function () {

    //Récupération des données Json de GetDataNCLine
    $.getJSON("/Home/GetDataNCLine", function (data) {
        $(function () {
            /* Initialisation des variables Month pour récupérer tous les mois
             * NCCount pour récupérer le nombre de NC par mois
             */
            var Month = [];
            var NCCount = [];

            /* Boucle pour parcourir toutes les données de data (recup Json)*/
            for (var i = 0; i < data.length; i++) {

                /* Switch pour convertir les mois en chiffre récupérer en mois textuel et les pusher dans le tableau month*/
                switch (data[i].name) {
                    case 1:
                        Month.push("Janvier");
                        break;
                    case 2:
                        Month.push("Février");
                        break;
                    case 3:
                        Month.push("Mars");
                        break;
                    case 4:
                        Month.push("Avril");
                        break;
                    case 5:
                        Month.push("Mai");
                        break;
                    case 6:
                        Month.push("Juin");
                        break;
                    case 7:
                        Month.push("Juillet");
                        break;
                    case 8:
                        Month.push("Août");
                        break;
                    case 9:
                        Month.push("Septembre");
                        break;
                    case 10:
                        Month.push("Octobre");
                        break;
                    case 11:
                        Month.push("Avril");
                        break;
                    default:
                        Month.push("Décembre");
                }
                //Le nombre de NC est pousser dans le tableau
                NCCount.push(data[i].count);
            }

            //-------------
            //- LINE CHART -
            //--------------
            var lineChartCanvas = $('#lineChart').get(0).getContext('2d');
            var lineChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: true,
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: true,
                        }
                    }]
                }
            }

            var lineChartData = {
                //Variable Month appelé pour donner dans le graphique
                labels: Month,
                datasets: [
                    {
                        label: 'Nombre NC',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: true,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        //Variable NCCount appelé
                        data: NCCount
                    }
                ]
            }
            lineChartData.datasets[0].fill = false
            lineChartOptions.datasetFill = false

            var lineChart = new Chart(lineChartCanvas, {
                type: 'line',
                data: lineChartData,
                options: lineChartOptions
            })

        })
                    //-------------
                    //- DONUT CHART -
                    //-------------
            $.getJSON("/Home/GetDataProduitNCDonut", function (data) {
                $(function () {

                    var nameProduit = [];
                    var QtProduit = [];
                    for (var i = 0; i < data.length; i++) {
                        nameProduit.push(data[i].name);
                        QtProduit.push(data[i].count);
                    }
                    
                    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
                    var donutData = {
                        labels: nameProduit,
                        datasets: [
                            {
                                data: QtProduit,
                                backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                            }
                        ]
                    }
                    var donutOptions = {
                        maintainAspectRatio: false,
                        responsive: true,
                    }

                    new Chart(donutChartCanvas, {
                        type: 'doughnut',
                        data: donutData,
                        options: donutOptions
                    })
                })
            //-------------
            //- PIE CHART -
            //-------------
                $.getJSON("/Home/GetDataResponsabilitePie", function (data) {
                    $(function () {

                        var nameResponsabilite = [];
                        var countResponsabilite = [];

                        for (var i = 0; i < data.length; i++) {
                            nameResponsabilite.push(data[i].name);
                            countResponsabilite.push(data[i].count);
                        }
                        
                        var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
                        var pieData = {
                            labels: nameResponsabilite,
                            datasets: [
                                {
                                    data: countResponsabilite,
                                    backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                                }
                            ]
                        }
                        var pieOptions = {
                            maintainAspectRatio: false,
                            responsive: true,
                        }

                        new Chart(pieChartCanvas, {
                            type: 'pie',
                            data: pieData,
                            options: pieOptions
                        })

                    })
                })

        })

    })

    //-------NonConformité Index au clic sur les états (_indexsommeNC) faire disparaitre les autres états----//

    $("#aTraiter").click(function () {
        $(".A").show("slow");
        $(".Fermer").hide("slow");
        $(".En").hide("slow");
        
    })

    $("#enCours").click(function () {
        $(".En").show();
        $(".Fermer").hide("slow");
        $(".A").hide("slow");
        
    })

    $("#fermer").click(function () {
        $(".Fermer").show();
        $(".En").hide("slow");
        $(".A").hide("slow");
        
    })

    $("#total").click(function () {
        $(".Fermer").show("slow");
        $(".En").show("slow");
        $(".A").show("slow");
    })



    //function animateCard() {
    //    $('.cardAnimate').animate({ deg: 360 },
    //        {
    //            duration: 2400,
    //            step: function (now) {
    //                $(this).css({ transform: 'rotate(' + now + 'deg)' })
    //            }
    //        })                 
            
    //}
    
    //animateCard();

    //function chartAnimate() {
    //    $('.chartAnimate').animate({ transX: "100px" },
    //        {
    //            duration: 2400,
    //            step: function (now) {
    //                $(this).css({ transform: "translateX(" + now + "transX)" })
    //            }
    //        })
    //        }
    //chartAnimate();
            

})

