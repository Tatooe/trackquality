﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetRoles] (
        [Id] nvarchar(450) NOT NULL,
        [Name] nvarchar(256) NULL,
        [NormalizedName] nvarchar(256) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetUsers] (
        [Id] nvarchar(450) NOT NULL,
        [UserName] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [Email] nvarchar(256) NULL,
        [NormalizedEmail] nvarchar(256) NULL,
        [EmailConfirmed] bit NOT NULL,
        [PasswordHash] nvarchar(max) NULL,
        [SecurityStamp] nvarchar(max) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [PhoneNumberConfirmed] bit NOT NULL,
        [TwoFactorEnabled] bit NOT NULL,
        [LockoutEnd] datetimeoffset NULL,
        [LockoutEnabled] bit NOT NULL,
        [AccessFailedCount] int NOT NULL,
        [Nom] nvarchar(50) NOT NULL,
        [Prenom] nvarchar(50) NOT NULL,
        CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [Produit] (
        [ProduitId] int NOT NULL IDENTITY,
        [Reference] nvarchar(max) NOT NULL,
        [Prix] float NOT NULL,
        CONSTRAINT [PK_Produit] PRIMARY KEY ([ProduitId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetRoleClaims] (
        [Id] int NOT NULL IDENTITY,
        [RoleId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetUserClaims] (
        [Id] int NOT NULL IDENTITY,
        [UserId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetUserLogins] (
        [LoginProvider] nvarchar(128) NOT NULL,
        [ProviderKey] nvarchar(128) NOT NULL,
        [ProviderDisplayName] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
        CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetUserRoles] (
        [UserId] nvarchar(450) NOT NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
        CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [AspNetUserTokens] (
        [UserId] nvarchar(450) NOT NULL,
        [LoginProvider] nvarchar(128) NOT NULL,
        [Name] nvarchar(128) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
        CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE TABLE [NonConformite] (
        [NonConformiteId] int NOT NULL IDENTITY,
        [NumeroNC] nvarchar(max) NOT NULL,
        [DescriptionNC] nvarchar(max) NOT NULL,
        [Photo] nvarchar(max) NULL,
        [DateNC] datetime2 NOT NULL,
        [QuantiteProdNC] int NOT NULL,
        [NumeroSerie] nvarchar(max) NOT NULL,
        [ProduitId] int NOT NULL,
        [UserId] nvarchar(max) NULL,
        [ApplicationAuthUserId] nvarchar(450) NULL,
        CONSTRAINT [PK_NonConformite] PRIMARY KEY ([NonConformiteId]),
        CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId] FOREIGN KEY ([ApplicationAuthUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_NonConformite_Produit_ProduitId] FOREIGN KEY ([ProduitId]) REFERENCES [Produit] ([ProduitId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite] ([ApplicationAuthUserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    CREATE INDEX [IX_NonConformite_ProduitId] ON [NonConformite] ([ProduitId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316144303_InitialCommit')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210316144303_InitialCommit', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE TABLE [EtatAction] (
        [EtatId] int NOT NULL IDENTITY,
        [Etat] int NOT NULL,
        CONSTRAINT [PK_EtatAction] PRIMARY KEY ([EtatId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE TABLE [Imputation] (
        [MImputationId] int NOT NULL IDENTITY,
        [ImputationNC] int NOT NULL,
        CONSTRAINT [PK_Imputation] PRIMARY KEY ([MImputationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE TABLE [AnalyseNC] (
        [AnalyseId] int NOT NULL IDENTITY,
        [MainDOeuvre] nvarchar(max) NOT NULL,
        [Milieu] nvarchar(max) NOT NULL,
        [Matiere] nvarchar(max) NOT NULL,
        [Methode] nvarchar(max) NOT NULL,
        [Materiel] nvarchar(max) NOT NULL,
        [NonConformiteId] int NOT NULL,
        [ImputationMImputationId] int NULL,
        CONSTRAINT [PK_AnalyseNC] PRIMARY KEY ([AnalyseId]),
        CONSTRAINT [FK_AnalyseNC_Imputation_ImputationMImputationId] FOREIGN KEY ([ImputationMImputationId]) REFERENCES [Imputation] ([MImputationId]) ON DELETE NO ACTION,
        CONSTRAINT [FK_AnalyseNC_NonConformite_NonConformiteId] FOREIGN KEY ([NonConformiteId]) REFERENCES [NonConformite] ([NonConformiteId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE TABLE [ActionNC] (
        [ActionId] int NOT NULL IDENTITY,
        [DescriptionAction] nvarchar(max) NULL,
        [DateAction] datetime2 NOT NULL,
        [AnalyseId] int NOT NULL,
        [AnalyseNCAnalyseId] int NULL,
        [EtatActionEtatId] int NULL,
        CONSTRAINT [PK_ActionNC] PRIMARY KEY ([ActionId]),
        CONSTRAINT [FK_ActionNC_AnalyseNC_AnalyseNCAnalyseId] FOREIGN KEY ([AnalyseNCAnalyseId]) REFERENCES [AnalyseNC] ([AnalyseId]) ON DELETE NO ACTION,
        CONSTRAINT [FK_ActionNC_EtatAction_EtatActionEtatId] FOREIGN KEY ([EtatActionEtatId]) REFERENCES [EtatAction] ([EtatId]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE INDEX [IX_ActionNC_AnalyseNCAnalyseId] ON [ActionNC] ([AnalyseNCAnalyseId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE INDEX [IX_ActionNC_EtatActionEtatId] ON [ActionNC] ([EtatActionEtatId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE INDEX [IX_AnalyseNC_ImputationMImputationId] ON [AnalyseNC] ([ImputationMImputationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    CREATE INDEX [IX_AnalyseNC_NonConformiteId] ON [AnalyseNC] ([NonConformiteId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316145140_AjoutEntiteDansContext')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210316145140_AjoutEntiteDansContext', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [ActionNC] DROP CONSTRAINT [FK_ActionNC_AnalyseNC_AnalyseNCAnalyseId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [ActionNC] DROP CONSTRAINT [FK_ActionNC_EtatAction_EtatActionEtatId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [AnalyseNC] DROP CONSTRAINT [FK_AnalyseNC_Imputation_ImputationMImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DROP INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [Imputation] DROP CONSTRAINT [PK_Imputation];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DROP INDEX [IX_AnalyseNC_ImputationMImputationId] ON [AnalyseNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DROP INDEX [IX_AnalyseNC_NonConformiteId] ON [AnalyseNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DROP INDEX [IX_ActionNC_AnalyseNCAnalyseId] ON [ActionNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DROP INDEX [IX_ActionNC_EtatActionEtatId] ON [ActionNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'ApplicationAuthUserId');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [NonConformite] DROP COLUMN [ApplicationAuthUserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DECLARE @var1 sysname;
    SELECT @var1 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Imputation]') AND [c].[name] = N'MImputationId');
    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Imputation] DROP CONSTRAINT [' + @var1 + '];');
    ALTER TABLE [Imputation] DROP COLUMN [MImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DECLARE @var2 sysname;
    SELECT @var2 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AnalyseNC]') AND [c].[name] = N'ImputationMImputationId');
    IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [AnalyseNC] DROP CONSTRAINT [' + @var2 + '];');
    ALTER TABLE [AnalyseNC] DROP COLUMN [ImputationMImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DECLARE @var3 sysname;
    SELECT @var3 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActionNC]') AND [c].[name] = N'AnalyseNCAnalyseId');
    IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [ActionNC] DROP CONSTRAINT [' + @var3 + '];');
    ALTER TABLE [ActionNC] DROP COLUMN [AnalyseNCAnalyseId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DECLARE @var4 sysname;
    SELECT @var4 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActionNC]') AND [c].[name] = N'EtatActionEtatId');
    IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [ActionNC] DROP CONSTRAINT [' + @var4 + '];');
    ALTER TABLE [ActionNC] DROP COLUMN [EtatActionEtatId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    DECLARE @var5 sysname;
    SELECT @var5 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'UserId');
    IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var5 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [UserId] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [Imputation] ADD [ImputationId] int NOT NULL IDENTITY;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [AnalyseNC] ADD [ImputationId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [ActionNC] ADD [EtatId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [Imputation] ADD CONSTRAINT [PK_Imputation] PRIMARY KEY ([ImputationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    CREATE INDEX [IX_NonConformite_UserId] ON [NonConformite] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    CREATE INDEX [IX_AnalyseNC_ImputationId] ON [AnalyseNC] ([ImputationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    CREATE UNIQUE INDEX [IX_AnalyseNC_NonConformiteId] ON [AnalyseNC] ([NonConformiteId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    CREATE UNIQUE INDEX [IX_ActionNC_AnalyseId] ON [ActionNC] ([AnalyseId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    CREATE INDEX [IX_ActionNC_EtatId] ON [ActionNC] ([EtatId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [ActionNC] ADD CONSTRAINT [FK_ActionNC_AnalyseNC_AnalyseId] FOREIGN KEY ([AnalyseId]) REFERENCES [AnalyseNC] ([AnalyseId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [ActionNC] ADD CONSTRAINT [FK_ActionNC_EtatAction_EtatId] FOREIGN KEY ([EtatId]) REFERENCES [EtatAction] ([EtatId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [AnalyseNC] ADD CONSTRAINT [FK_AnalyseNC_Imputation_ImputationId] FOREIGN KEY ([ImputationId]) REFERENCES [Imputation] ([ImputationId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316151544_AjoutdeForeignKey')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210316151544_AjoutdeForeignKey', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316161016_AjoutValidationDeDonneeEtContraintes')
BEGIN
    DECLARE @var6 sysname;
    SELECT @var6 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Produit]') AND [c].[name] = N'Reference');
    IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [Produit] DROP CONSTRAINT [' + @var6 + '];');
    ALTER TABLE [Produit] ALTER COLUMN [Reference] nvarchar(50) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316161016_AjoutValidationDeDonneeEtContraintes')
BEGIN
    DECLARE @var7 sysname;
    SELECT @var7 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'Photo');
    IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var7 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [Photo] nvarchar(250) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316161016_AjoutValidationDeDonneeEtContraintes')
BEGIN
    DECLARE @var8 sysname;
    SELECT @var8 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'NumeroSerie');
    IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var8 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [NumeroSerie] nvarchar(50) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316161016_AjoutValidationDeDonneeEtContraintes')
BEGIN
    DECLARE @var9 sysname;
    SELECT @var9 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'NumeroNC');
    IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var9 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [NumeroNC] nvarchar(11) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316161016_AjoutValidationDeDonneeEtContraintes')
BEGIN
    DECLARE @var10 sysname;
    SELECT @var10 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActionNC]') AND [c].[name] = N'DescriptionAction');
    IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [ActionNC] DROP CONSTRAINT [' + @var10 + '];');
    ALTER TABLE [ActionNC] ALTER COLUMN [DescriptionAction] nvarchar(max) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210316161016_AjoutValidationDeDonneeEtContraintes')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210316161016_AjoutValidationDeDonneeEtContraintes', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [ActionNC] DROP CONSTRAINT [FK_ActionNC_AnalyseNC_AnalyseId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [ActionNC] DROP CONSTRAINT [FK_ActionNC_EtatAction_EtatId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [EtatAction] DROP CONSTRAINT [PK_EtatAction];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    DROP INDEX [IX_AnalyseNC_NonConformiteId] ON [AnalyseNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    DROP INDEX [IX_ActionNC_AnalyseId] ON [ActionNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    DROP INDEX [IX_ActionNC_EtatId] ON [ActionNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    DECLARE @var11 sysname;
    SELECT @var11 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EtatAction]') AND [c].[name] = N'EtatId');
    IF @var11 IS NOT NULL EXEC(N'ALTER TABLE [EtatAction] DROP CONSTRAINT [' + @var11 + '];');
    ALTER TABLE [EtatAction] DROP COLUMN [EtatId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    DECLARE @var12 sysname;
    SELECT @var12 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActionNC]') AND [c].[name] = N'AnalyseId');
    IF @var12 IS NOT NULL EXEC(N'ALTER TABLE [ActionNC] DROP CONSTRAINT [' + @var12 + '];');
    ALTER TABLE [ActionNC] DROP COLUMN [AnalyseId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    DECLARE @var13 sysname;
    SELECT @var13 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActionNC]') AND [c].[name] = N'EtatId');
    IF @var13 IS NOT NULL EXEC(N'ALTER TABLE [ActionNC] DROP CONSTRAINT [' + @var13 + '];');
    ALTER TABLE [ActionNC] DROP COLUMN [EtatId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [NonConformite] ADD [EtatActionId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [EtatAction] ADD [EtatActionId] int NOT NULL IDENTITY;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [AnalyseNC] ADD [ActionId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [EtatAction] ADD CONSTRAINT [PK_EtatAction] PRIMARY KEY ([EtatActionId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    CREATE INDEX [IX_NonConformite_EtatActionId] ON [NonConformite] ([EtatActionId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    CREATE INDEX [IX_AnalyseNC_ActionId] ON [AnalyseNC] ([ActionId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    CREATE INDEX [IX_AnalyseNC_NonConformiteId] ON [AnalyseNC] ([NonConformiteId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [AnalyseNC] ADD CONSTRAINT [FK_AnalyseNC_ActionNC_ActionId] FOREIGN KEY ([ActionId]) REFERENCES [ActionNC] ([ActionId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_EtatAction_EtatActionId] FOREIGN KEY ([EtatActionId]) REFERENCES [EtatAction] ([EtatActionId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210322152126_modificationRelationEtat')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210322152126_modificationRelationEtat', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_AspNetUsers_UserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    DROP INDEX [IX_NonConformite_UserId] ON [NonConformite];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    DECLARE @var14 sysname;
    SELECT @var14 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'UserId');
    IF @var14 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var14 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [UserId] nvarchar(max) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    ALTER TABLE [NonConformite] ADD [ApplicationAuthUserId] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    CREATE INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite] ([ApplicationAuthUserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId] FOREIGN KEY ([ApplicationAuthUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323112533_foreignKeySupprimerDansNc')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210323112533_foreignKeySupprimerDansNc', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323151644_ajoutEnumEtatDansBDD')
BEGIN
    DECLARE @var15 sysname;
    SELECT @var15 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EtatAction]') AND [c].[name] = N'Etat');
    IF @var15 IS NOT NULL EXEC(N'ALTER TABLE [EtatAction] DROP CONSTRAINT [' + @var15 + '];');
    ALTER TABLE [EtatAction] ALTER COLUMN [Etat] nvarchar(max) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323151644_ajoutEnumEtatDansBDD')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210323151644_ajoutEnumEtatDansBDD', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323152025_retourEnArriere')
BEGIN
    DECLARE @var16 sysname;
    SELECT @var16 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EtatAction]') AND [c].[name] = N'Etat');
    IF @var16 IS NOT NULL EXEC(N'ALTER TABLE [EtatAction] DROP CONSTRAINT [' + @var16 + '];');
    ALTER TABLE [EtatAction] ALTER COLUMN [Etat] int NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210323152025_retourEnArriere')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210323152025_retourEnArriere', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324140316_modificationEnStringEnumEtat')
BEGIN
    DECLARE @var17 sysname;
    SELECT @var17 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EtatAction]') AND [c].[name] = N'Etat');
    IF @var17 IS NOT NULL EXEC(N'ALTER TABLE [EtatAction] DROP CONSTRAINT [' + @var17 + '];');
    ALTER TABLE [EtatAction] ALTER COLUMN [Etat] nvarchar(max) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324140316_modificationEnStringEnumEtat')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210324140316_modificationEnStringEnumEtat', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_EtatAction_EtatActionId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    DROP INDEX [IX_NonConformite_EtatActionId] ON [NonConformite];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    ALTER TABLE [EtatAction] DROP CONSTRAINT [PK_EtatAction];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    DECLARE @var18 sysname;
    SELECT @var18 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'EtatActionId');
    IF @var18 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var18 + '];');
    ALTER TABLE [NonConformite] DROP COLUMN [EtatActionId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    DECLARE @var19 sysname;
    SELECT @var19 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EtatAction]') AND [c].[name] = N'EtatActionId');
    IF @var19 IS NOT NULL EXEC(N'ALTER TABLE [EtatAction] DROP CONSTRAINT [' + @var19 + '];');
    ALTER TABLE [EtatAction] DROP COLUMN [EtatActionId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    ALTER TABLE [NonConformite] ADD [EtatActionEtat] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    DECLARE @var20 sysname;
    SELECT @var20 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EtatAction]') AND [c].[name] = N'Etat');
    IF @var20 IS NOT NULL EXEC(N'ALTER TABLE [EtatAction] DROP CONSTRAINT [' + @var20 + '];');
    ALTER TABLE [EtatAction] ALTER COLUMN [Etat] nvarchar(450) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    ALTER TABLE [EtatAction] ADD CONSTRAINT [PK_EtatAction] PRIMARY KEY ([Etat]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    CREATE INDEX [IX_NonConformite_EtatActionEtat] ON [NonConformite] ([EtatActionEtat]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_EtatAction_EtatActionEtat] FOREIGN KEY ([EtatActionEtat]) REFERENCES [EtatAction] ([Etat]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144457_clefetrangereEtatActionSupprime')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210324144457_clefetrangereEtatActionSupprime', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    ALTER TABLE [AnalyseNC] DROP CONSTRAINT [FK_AnalyseNC_Imputation_ImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    ALTER TABLE [Imputation] DROP CONSTRAINT [PK_Imputation];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    DROP INDEX [IX_AnalyseNC_ImputationId] ON [AnalyseNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    DECLARE @var21 sysname;
    SELECT @var21 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Imputation]') AND [c].[name] = N'ImputationId');
    IF @var21 IS NOT NULL EXEC(N'ALTER TABLE [Imputation] DROP CONSTRAINT [' + @var21 + '];');
    ALTER TABLE [Imputation] DROP COLUMN [ImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    DECLARE @var22 sysname;
    SELECT @var22 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AnalyseNC]') AND [c].[name] = N'ImputationId');
    IF @var22 IS NOT NULL EXEC(N'ALTER TABLE [AnalyseNC] DROP CONSTRAINT [' + @var22 + '];');
    ALTER TABLE [AnalyseNC] DROP COLUMN [ImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    DECLARE @var23 sysname;
    SELECT @var23 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Imputation]') AND [c].[name] = N'ImputationNC');
    IF @var23 IS NOT NULL EXEC(N'ALTER TABLE [Imputation] DROP CONSTRAINT [' + @var23 + '];');
    ALTER TABLE [Imputation] ALTER COLUMN [ImputationNC] nvarchar(450) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    ALTER TABLE [AnalyseNC] ADD [ImputationNC] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    ALTER TABLE [Imputation] ADD CONSTRAINT [PK_Imputation] PRIMARY KEY ([ImputationNC]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    CREATE INDEX [IX_AnalyseNC_ImputationNC] ON [AnalyseNC] ([ImputationNC]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    ALTER TABLE [AnalyseNC] ADD CONSTRAINT [FK_AnalyseNC_Imputation_ImputationNC] FOREIGN KEY ([ImputationNC]) REFERENCES [Imputation] ([ImputationNC]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324144923_suppressionClefEtrangereImputation')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210324144923_suppressionClefEtrangereImputation', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151152_ajoutDeRequiredDansNonConformite')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_EtatAction_EtatActionEtat];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151152_ajoutDeRequiredDansNonConformite')
BEGIN
    DECLARE @var24 sysname;
    SELECT @var24 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'UserId');
    IF @var24 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var24 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [UserId] nvarchar(max) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151152_ajoutDeRequiredDansNonConformite')
BEGIN
    DROP INDEX [IX_NonConformite_EtatActionEtat] ON [NonConformite];
    DECLARE @var25 sysname;
    SELECT @var25 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'EtatActionEtat');
    IF @var25 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var25 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [EtatActionEtat] nvarchar(450) NOT NULL;
    CREATE INDEX [IX_NonConformite_EtatActionEtat] ON [NonConformite] ([EtatActionEtat]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151152_ajoutDeRequiredDansNonConformite')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_EtatAction_EtatActionEtat] FOREIGN KEY ([EtatActionEtat]) REFERENCES [EtatAction] ([Etat]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151152_ajoutDeRequiredDansNonConformite')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210324151152_ajoutDeRequiredDansNonConformite', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    ALTER TABLE [AnalyseNC] DROP CONSTRAINT [FK_AnalyseNC_Imputation_ImputationNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    DECLARE @var26 sysname;
    SELECT @var26 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'Photo');
    IF @var26 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var26 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [Photo] nvarchar(250) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    DROP INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite];
    DECLARE @var27 sysname;
    SELECT @var27 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'ApplicationAuthUserId');
    IF @var27 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var27 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [ApplicationAuthUserId] nvarchar(450) NOT NULL;
    CREATE INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite] ([ApplicationAuthUserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    DROP INDEX [IX_AnalyseNC_ImputationNC] ON [AnalyseNC];
    DECLARE @var28 sysname;
    SELECT @var28 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AnalyseNC]') AND [c].[name] = N'ImputationNC');
    IF @var28 IS NOT NULL EXEC(N'ALTER TABLE [AnalyseNC] DROP CONSTRAINT [' + @var28 + '];');
    ALTER TABLE [AnalyseNC] ALTER COLUMN [ImputationNC] nvarchar(450) NOT NULL;
    CREATE INDEX [IX_AnalyseNC_ImputationNC] ON [AnalyseNC] ([ImputationNC]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    ALTER TABLE [AnalyseNC] ADD CONSTRAINT [FK_AnalyseNC_Imputation_ImputationNC] FOREIGN KEY ([ImputationNC]) REFERENCES [Imputation] ([ImputationNC]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId] FOREIGN KEY ([ApplicationAuthUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210324151607_AjoutRequiredDansTousLesModelsCrees')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210324151607_AjoutRequiredDansTousLesModelsCrees', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325105115_suppressionEnumDsEtatActionEtImputation')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210325105115_suppressionEnumDsEtatActionEtImputation', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325134406_majUserIdDansNC')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325134406_majUserIdDansNC')
BEGIN
    DECLARE @var29 sysname;
    SELECT @var29 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'ApplicationAuthUserId');
    IF @var29 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var29 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [ApplicationAuthUserId] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325134406_majUserIdDansNC')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId] FOREIGN KEY ([ApplicationAuthUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325134406_majUserIdDansNC')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210325134406_majUserIdDansNC', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135147_majrelationdansnc')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_EtatAction_EtatActionEtat];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135147_majrelationdansnc')
BEGIN
    DECLARE @var30 sysname;
    SELECT @var30 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'UserId');
    IF @var30 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var30 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [UserId] nvarchar(max) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135147_majrelationdansnc')
BEGIN
    DECLARE @var31 sysname;
    SELECT @var31 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'EtatActionEtat');
    IF @var31 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var31 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [EtatActionEtat] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135147_majrelationdansnc')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_EtatAction_EtatActionEtat] FOREIGN KEY ([EtatActionEtat]) REFERENCES [EtatAction] ([Etat]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135147_majrelationdansnc')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210325135147_majrelationdansnc', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135704_correctionModelNC')
BEGIN
    DECLARE @var32 sysname;
    SELECT @var32 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'UserId');
    IF @var32 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var32 + '];');
    ALTER TABLE [NonConformite] DROP COLUMN [UserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325135704_correctionModelNC')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210325135704_correctionModelNC', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_EtatAction_EtatActionEtat];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    DROP INDEX [IX_NonConformite_EtatActionEtat] ON [NonConformite];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    DECLARE @var33 sysname;
    SELECT @var33 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'EtatActionEtat');
    IF @var33 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var33 + '];');
    ALTER TABLE [NonConformite] DROP COLUMN [EtatActionEtat];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    ALTER TABLE [NonConformite] ADD [EtatActionId] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    CREATE INDEX [IX_NonConformite_EtatActionId] ON [NonConformite] ([EtatActionId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_EtatAction_EtatActionId] FOREIGN KEY ([EtatActionId]) REFERENCES [EtatAction] ([Etat]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210325142300_correctionModelNCIdEtatAction')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210325142300_correctionModelNCIdEtatAction', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327134418_changementStringToBytePhotoNC')
BEGIN
    DECLARE @var34 sysname;
    SELECT @var34 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'Photo');
    IF @var34 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var34 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [Photo] nvarchar(250) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327134418_changementStringToBytePhotoNC')
BEGIN
    ALTER TABLE [NonConformite] ADD [NumeroNCIncrement] nvarchar(max) NOT NULL DEFAULT N'';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327134418_changementStringToBytePhotoNC')
BEGIN
    ALTER TABLE [NonConformite] ADD [PhotoNC] varbinary(250) NOT NULL DEFAULT 0x;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327134418_changementStringToBytePhotoNC')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210327134418_changementStringToBytePhotoNC', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327140321_supressionphotonnformite')
BEGIN
    DECLARE @var35 sysname;
    SELECT @var35 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'Photo');
    IF @var35 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var35 + '];');
    ALTER TABLE [NonConformite] DROP COLUMN [Photo];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327140321_supressionphotonnformite')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210327140321_supressionphotonnformite', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327142609_photobytotostring')
BEGIN
    DECLARE @var36 sysname;
    SELECT @var36 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'PhotoNC');
    IF @var36 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var36 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [PhotoNC] nvarchar(250) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327142609_photobytotostring')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210327142609_photobytotostring', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327155757_correctionerreurnumNCincrementstringtoint')
BEGIN
    DECLARE @var37 sysname;
    SELECT @var37 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'NumeroNCIncrement');
    IF @var37 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var37 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [NumeroNCIncrement] int NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210327155757_correctionerreurnumNCincrementstringtoint')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210327155757_correctionerreurnumNCincrementstringtoint', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    ALTER TABLE [AnalyseNC] DROP CONSTRAINT [FK_AnalyseNC_ActionNC_ActionId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    ALTER TABLE [AnalyseNC] DROP CONSTRAINT [FK_AnalyseNC_Imputation_ImputationNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    DROP INDEX [IX_AnalyseNC_ActionId] ON [AnalyseNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    DROP INDEX [IX_AnalyseNC_ImputationNC] ON [AnalyseNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    DECLARE @var38 sysname;
    SELECT @var38 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AnalyseNC]') AND [c].[name] = N'ActionId');
    IF @var38 IS NOT NULL EXEC(N'ALTER TABLE [AnalyseNC] DROP CONSTRAINT [' + @var38 + '];');
    ALTER TABLE [AnalyseNC] DROP COLUMN [ActionId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    DECLARE @var39 sysname;
    SELECT @var39 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AnalyseNC]') AND [c].[name] = N'ImputationNC');
    IF @var39 IS NOT NULL EXEC(N'ALTER TABLE [AnalyseNC] DROP CONSTRAINT [' + @var39 + '];');
    ALTER TABLE [AnalyseNC] DROP COLUMN [ImputationNC];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    DECLARE @var40 sysname;
    SELECT @var40 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'PhotoNC');
    IF @var40 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var40 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [PhotoNC] nvarchar(250) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    ALTER TABLE [AnalyseNC] ADD [ImputationId] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    CREATE INDEX [IX_AnalyseNC_ImputationId] ON [AnalyseNC] ([ImputationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    ALTER TABLE [AnalyseNC] ADD CONSTRAINT [FK_AnalyseNC_Imputation_ImputationId] FOREIGN KEY ([ImputationId]) REFERENCES [Imputation] ([ImputationNC]) ON DELETE NO ACTION;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210328133303_correctionAnalyseNC')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210328133303_correctionAnalyseNC', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210329084726_AjoutDateDansAnalyseAction')
BEGIN
    ALTER TABLE [AnalyseNC] ADD [DateAnalyse] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210329084726_AjoutDateDansAnalyseAction')
BEGIN
    ALTER TABLE [ActionNC] ADD [DateAnalyse] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210329084726_AjoutDateDansAnalyseAction')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210329084726_AjoutDateDansAnalyseAction', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210329175041_suppressionDoublonDateDansAction')
BEGIN
    DECLARE @var41 sysname;
    SELECT @var41 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActionNC]') AND [c].[name] = N'DateAnalyse');
    IF @var41 IS NOT NULL EXEC(N'ALTER TABLE [ActionNC] DROP CONSTRAINT [' + @var41 + '];');
    ALTER TABLE [ActionNC] DROP COLUMN [DateAnalyse];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210329175041_suppressionDoublonDateDansAction')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210329175041_suppressionDoublonDateDansAction', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210330073222_relationActionVSAnalyse')
BEGIN
    ALTER TABLE [ActionNC] ADD [analyseNCId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210330073222_relationActionVSAnalyse')
BEGIN
    CREATE INDEX [IX_ActionNC_analyseNCId] ON [ActionNC] ([analyseNCId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210330073222_relationActionVSAnalyse')
BEGIN
    ALTER TABLE [ActionNC] ADD CONSTRAINT [FK_ActionNC_AnalyseNC_analyseNCId] FOREIGN KEY ([analyseNCId]) REFERENCES [AnalyseNC] ([AnalyseId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210330073222_relationActionVSAnalyse')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210330073222_relationActionVSAnalyse', N'3.1.13');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    ALTER TABLE [AnalyseNC] DROP CONSTRAINT [FK_AnalyseNC_Imputation_ImputationId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    ALTER TABLE [NonConformite] DROP CONSTRAINT [FK_NonConformite_EtatAction_EtatActionId];
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    DROP INDEX [IX_NonConformite_EtatActionId] ON [NonConformite];
    DECLARE @var42 sysname;
    SELECT @var42 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'EtatActionId');
    IF @var42 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var42 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [EtatActionId] nvarchar(450) NOT NULL;
    CREATE INDEX [IX_NonConformite_EtatActionId] ON [NonConformite] ([EtatActionId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    DROP INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite];
    DECLARE @var43 sysname;
    SELECT @var43 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[NonConformite]') AND [c].[name] = N'ApplicationAuthUserId');
    IF @var43 IS NOT NULL EXEC(N'ALTER TABLE [NonConformite] DROP CONSTRAINT [' + @var43 + '];');
    ALTER TABLE [NonConformite] ALTER COLUMN [ApplicationAuthUserId] nvarchar(450) NOT NULL;
    CREATE INDEX [IX_NonConformite_ApplicationAuthUserId] ON [NonConformite] ([ApplicationAuthUserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    DROP INDEX [IX_AnalyseNC_ImputationId] ON [AnalyseNC];
    DECLARE @var44 sysname;
    SELECT @var44 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AnalyseNC]') AND [c].[name] = N'ImputationId');
    IF @var44 IS NOT NULL EXEC(N'ALTER TABLE [AnalyseNC] DROP CONSTRAINT [' + @var44 + '];');
    ALTER TABLE [AnalyseNC] ALTER COLUMN [ImputationId] nvarchar(450) NOT NULL;
    CREATE INDEX [IX_AnalyseNC_ImputationId] ON [AnalyseNC] ([ImputationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    ALTER TABLE [AnalyseNC] ADD CONSTRAINT [FK_AnalyseNC_Imputation_ImputationId] FOREIGN KEY ([ImputationId]) REFERENCES [Imputation] ([ImputationNC]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_AspNetUsers_ApplicationAuthUserId] FOREIGN KEY ([ApplicationAuthUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    ALTER TABLE [NonConformite] ADD CONSTRAINT [FK_NonConformite_EtatAction_EtatActionId] FOREIGN KEY ([EtatActionId]) REFERENCES [EtatAction] ([Etat]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210406145917_essaiazure')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210406145917_essaiazure', N'3.1.13');
END;

GO

