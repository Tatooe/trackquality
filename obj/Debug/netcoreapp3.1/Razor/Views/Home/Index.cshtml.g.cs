#pragma checksum "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "123157c73d33cfafa978e0153be4c82a70ed0091"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\_ViewImports.cshtml"
using TrackQuality;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\_ViewImports.cshtml"
using TrackQuality.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"123157c73d33cfafa978e0153be4c82a70ed0091", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55c9fd34a3f2ca43d28f0aa4ff34088c9c2d8afb", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<section class=""col-12 col-md-10 mx-auto mt-5 d-block "">
    <h1 class=""text-center my-4 py-2 bg-light shadow-sm titre"">Indicateurs qualité année 2021</h1>           

<div class=""container-fluid"">



    <div class=""d-flex flex-column flex-md-row justify-content-around my-4"">


        <div class=""info-box mx-md-2 my-md-4"">
            <span class=""info-box-icon bg-info elevation-1""><i class=""fas fa-calculator""></i></span>

            <div class=""info-box-content"">
                <span class=""info-box-text"">Total NC</span>
                <span class=""info-box-number"">
                    ");
#nullable restore
#line 21 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
               Write(ViewBag.TotalNC);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </span>
            </div>
        </div>

        <div class=""info-box mx-md-2 my-md-4"">
            <span class=""info-box-icon bg-info elevation-1""><i class=""fas fa-box""></i></span>

            <div class=""info-box-content"">
                <span class=""info-box-text"">Nombre de produits NC</span>
                <span class=""info-box-number"">
                    ");
#nullable restore
#line 32 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
               Write(ViewBag.NbProduitsNC);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </span>
            </div>
        </div>

        <div class=""info-box mx-md-2 my-md-4"">
            <span class=""info-box-icon bg-info elevation-1""><i class=""fas fa-euro-sign""></i></span>

            <div class=""info-box-content"">
                <span class=""info-box-text"">Coût Total</span>
                <span class=""info-box-number"">
                    ");
#nullable restore
#line 43 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
               Write(ViewBag.CoutNC);

#line default
#line hidden
#nullable disable
            WriteLiteral(@" €
                </span>
            </div>
        </div>

        <div class=""info-box mx-md-2 my-md-4"">
            <span class=""info-box-icon bg-gradient-warning elevation-1""><i class=""fas fa-exclamation-triangle""></i></span>

            <div class=""info-box-content"">
                <span class=""info-box-text"">Top Produit NC</span>
                <span class=""info-box-number"">
                    ");
#nullable restore
#line 54 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
               Write(ViewBag.TopProduitNC);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </span>
            </div>
        </div>

        <div class=""info-box mx-md-2 my-md-4"">
            <span class=""info-box-icon bg-gradient-warning elevation-1""><i class=""fas fa-exclamation-triangle""></i></span>

            <div class=""info-box-content"">
                <span class=""info-box-text"">Top Responsabilité</span>
                <span class=""info-box-number"">
                    ");
#nullable restore
#line 65 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
               Write(ViewBag.TopResponsabiliteNC);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </span>
            </div>
        </div>

        <div class=""info-box mx-md-2 my-md-4"">
            <span class=""info-box-icon bg-gradient-danger elevation-1""><i class=""fas fa-trash""></i></span>

            <div class=""info-box-content"">
                <span class=""info-box-text"">Quantité Rébuté</span>
                <span class=""info-box-number"">
                    ");
#nullable restore
#line 76 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\Home\Index.cshtml"
               Write(ViewBag.NbProduitRebute);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                </span>
            </div>
        </div>


    </div>

    <div class=""d-flex flex-column flex-md-row mb-5 chartAnimate barAnum"">

        <div class=""col-12 col-md-6"">
            <!-- LINE CHART -->
            <div class=""card card-info"">
                <div class=""card-header"">
                    <h3 class=""card-title text-center"">Nombre de non-conformités par mois</h3>
                </div>

                <div class=""card-body"">
                    <div class=""chart"">
                        <div class=""chartjs-size-monitor"">
                            <div class=""chartjs-size-monitor-expand"">
                                <div");
            BeginWriteAttribute("class", " class=\"", 3506, "\"", 3514, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"chartjs-size-monitor-shrink\">\r\n                                <div");
            BeginWriteAttribute("class", " class=\"", 3703, "\"", 3711, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                                </div>
                            </div>
                        </div>
                        <canvas id=""lineChart"" style=""min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 604px;"" width=""604"" height=""250"" class=""chartjs-render-monitor""></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>


        <div class=""col-12 col-md-3"">
            <!-- DONUT CHART -->
            <div class=""card card-info"">
                <div class=""card-header"">
                    <h3 class=""card-title"">Produits</h3>
                </div>
                <div class=""card-body"">
                    <div class=""chartjs-size-monitor"">
                    <div class=""chartjs-size-monitor-expand"">
                        <div");
            BeginWriteAttribute("class", " class=\"", 4603, "\"", 4611, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                            </div>\r\n                        </div>\r\n                    <div class=\"chartjs-size-monitor-shrink\">\r\n                        <div");
            BeginWriteAttribute("class", " class=\"", 4774, "\"", 4782, 0);
            EndWriteAttribute();
            WriteLiteral(@"></div>
                        </div>
                    </div>
                    <canvas id=""donutChart"" style=""min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 604px;"" width=""604"" height=""250"" class=""chartjs-render-monitor""></canvas>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

        <div class=""col-12 col-md-3"">
            <!-- PIE CHART -->
            <div class=""card card-info"">
                <div class=""card-header"">
                    <h3 class=""card-title"">Responsabilités</h3>
                </div>
                <div class=""card-body"">
                    <div class=""chartjs-size-monitor"">
                    <div class=""chartjs-size-monitor-expand"">
                        <div");
            BeginWriteAttribute("class", " class=\"", 5633, "\"", 5641, 0);
            EndWriteAttribute();
            WriteLiteral("></div>\r\n                        </div>\r\n                    <div class=\"chartjs-size-monitor-shrink\">\r\n                        <div");
            BeginWriteAttribute("class", " class=\"", 5774, "\"", 5782, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                            </div>
                        </div>
                    </div>
                    <canvas id=""pieChart"" style=""min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 604px;"" width=""604"" height=""250"" class=""chartjs-render-monitor""></canvas>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>



    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
</section>


");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
