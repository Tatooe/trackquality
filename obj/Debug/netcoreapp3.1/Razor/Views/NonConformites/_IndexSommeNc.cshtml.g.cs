#pragma checksum "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\NonConformites\_IndexSommeNc.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "50b0434a7e6437874c875b99708c232764f81cdd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_NonConformites__IndexSommeNc), @"mvc.1.0.view", @"/Views/NonConformites/_IndexSommeNc.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\_ViewImports.cshtml"
using TrackQuality;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\_ViewImports.cshtml"
using TrackQuality.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"50b0434a7e6437874c875b99708c232764f81cdd", @"/Views/NonConformites/_IndexSommeNc.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"55c9fd34a3f2ca43d28f0aa4ff34088c9c2d8afb", @"/Views/_ViewImports.cshtml")]
    public class Views_NonConformites__IndexSommeNc : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n\r\n");
            WriteLiteral(@"
                <div id=""total"" class=""col-12 col-sm-6 col-md-2"">
                    <div class=""info-box"">
                        <span class=""info-box-icon bg-info elevation-1""><i class=""fas fa-calculator""></i></span>

                        <div class=""info-box-content"">
                            <span class=""info-box-text"">Total</span>
                            <span class=""info-box-number"">
                                ");
#nullable restore
#line 17 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\NonConformites\_IndexSommeNc.cshtml"
                           Write(ViewBag.NbNcTotal);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </span>
                        </div>
                    </div>
                </div>

                <div id=""aTraiter"" class=""col-12 col-sm-6 col-md-2"">
                    <div class=""info-box"">
                        <span class=""info-box-icon bg-danger elevation-1""><i class=""fas fa-hourglass-start""></i></span>

                        <div class=""info-box-content"">
                            <span class=""info-box-text"">A traiter</span>
                            <span class=""info-box-number"">
                                ");
#nullable restore
#line 30 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\NonConformites\_IndexSommeNc.cshtml"
                           Write(ViewBag.NbNcATraiter);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </span>
                        </div>
                    </div>
                </div>


                <div id=""enCours"" class=""col-12 col-sm-6 col-md-2"">
                    <div class=""info-box"">
                        <span class=""info-box-icon bg-warning elevation-1""><i class=""fas fa-spinner""></i></span>

                        <div class=""info-box-content"">
                            <span class=""info-box-text"">En cours</span>
                            <span class=""info-box-number"">
                                ");
#nullable restore
#line 44 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\NonConformites\_IndexSommeNc.cshtml"
                           Write(ViewBag.NbNcEnCours);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                            </span>
                        </div>
                    </div>
                </div>

                <div id=""fermer"" class=""col-12 col-sm-6 col-md-2"">
                    <div class=""info-box"">
                        <span class=""info-box-icon bg-success elevation-1""><i class=""fas fa-check-square""></i></span>

                        <div class=""info-box-content"">
                            <span class=""info-box-text"">Fermer</span>
                            <span class=""info-box-number"">
                                ");
#nullable restore
#line 57 "C:\Users\tatia\Documents\code\C#\TrackQuality\Views\NonConformites\_IndexSommeNc.cshtml"
                           Write(ViewBag.NbNcFermer);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n\r\n");
            WriteLiteral("    \r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
