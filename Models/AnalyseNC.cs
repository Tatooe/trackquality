﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrackQuality.Models
{
    public class AnalyseNC
    {
        [Key]
        public int AnalyseId { get; set; }

        [Required(ErrorMessage = "La date est obligatoire.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime DateAnalyse { get; set; }

        [Required(ErrorMessage = "Merci de renseigner l'analyse {0}")]
        [DataType(DataType.Text)]
        [Display(Name = "Main d'oeuvre")]
        public string MainDOeuvre { get; set; }

        [Required(ErrorMessage = "Merci de renseigner l'analyse {0}")]
        [DataType(DataType.Text)]
        [Display(Name = "Milieu")]
        public string Milieu { get; set; }

        [Required(ErrorMessage = "Merci de renseigner l'analyse {0}")]
        [DataType(DataType.Text)]
        [Display(Name = "Matière")]
        public string Matiere { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Méthode")]
        [Required(ErrorMessage = "Merci de renseigner l'analyse {0}")]
        public string Methode { get; set; }

        [Required(ErrorMessage = "Merci de renseigner l'analyse {0}")]
        [DataType(DataType.Text)]
        [Display(Name = "Matériel")]
        public string Materiel { get; set; }

        //Relation 1 à 1 avec non conformité
        [Required]
        [ForeignKey("NonConformite")]
        public int NonConformiteId { get; set; }
        public NonConformite NonConformite { get; set; }

        //Relation avec Imputation
        [Required]
        [Display(Name = "Imputation")]
        public string ImputationId { get; set; }
        public Imputation Imputation { get; set; }

    }
}
