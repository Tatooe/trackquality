﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TrackQuality.Models
{
    public class Produit
    {
        [Key]
        public int ProduitId { get; set; }

        [Required(ErrorMessage ="Une {0} de rpoduit est obligatoire")]
        [StringLength(50, ErrorMessage = "Le {0} ne peut pas excéder {1} caractères. ")]
        [Display(Name = "Référence")]
        public string Reference { get; set; }

        [Required(ErrorMessage = "Un {0} est obligatoire")]
        [Range(0, int.MaxValue, ErrorMessage = "Le montant du produit doit être un chiffre positif")]
        [Display(Name = "Prix unitaire en €")]
        public double Prix { get; set; }

        //Relation avec NonConformité 1 produit peut avoir plusieurs NonConformite
        public ICollection<NonConformite> NonConformites { get; set; }
    }
}
