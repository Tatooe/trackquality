﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrackQuality.Models
{

    public class EtatAction
    {
        [Key]

        [Required(ErrorMessage = "Un état doit être renseigné")]

        public string Etat { get; set; } = "encours";

        //Relation avec NonConformité
        public ICollection<NonConformite> NonConformites { get; set; }
    }
}
