﻿
using System;
using System.ComponentModel.DataAnnotations;
using TrackQuality.Areas.Identity.Data;

namespace TrackQuality.Models
{
    public class NonConformite
    {
        [Key]
        public int NonConformiteId { get; set; }

        [Required(ErrorMessage = "Le {0} est obligatoire.")]        
        public int NumeroNCIncrement { get; set; }

        [Required(ErrorMessage = "Le {0} est obligatoire.")]
        [StringLength(11, ErrorMessage = "Le {0} ne peut pas excéder {1} caractères. ")]
        [Display(Name = "N°")]
        public string NumeroNC { get; set; } 

        [Required(ErrorMessage = "Une description de la non-conformité est obligatoire.")]
        [DataType(DataType.Text)]
        [Display(Name = "Description")]
        public string DescriptionNC { get; set; }


        //[Required(ErrorMessage = "Une photo doit être inséré")]
        [StringLength(250, ErrorMessage = "Merci de renommer votre photo, le nom est trop long.")]
        [Display(Name = "Photo")]
        public string PhotoNC { get; set; }

        [Required(ErrorMessage = "La date est obligatoire.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime DateNC { get; set; }

        [Required(ErrorMessage = "La quantité est obligatoire.")]
        [Range(0, int.MaxValue, ErrorMessage = "La quantité doit être supérieur à 0")]
        [Display(Name = "Quantité")]
        public int QuantiteProdNC { get; set; }

        [Required(ErrorMessage = "Un ou des numéros de série doivent être obligatoirement renseigné.")]
        [StringLength(50, ErrorMessage = "Le {0} ne peut pas excéder {1} caractères. ")]
        [Display(Name = "Numéro de série")]
        public string NumeroSerie { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "La quantité doit être supérieur à 0")]
        [Display(Name = "Quantité Rebuté")]
        public int QtRebute { get; set; }

        //Relation avec le produit 1 NC peut être attribué à un seul produit ManyToOne

        [Required(ErrorMessage = "Un produit doit être choisi")]
        public int ProduitId { get; set; }
        [Display(Name = "Référence")]
        public Produit Produit { get; set; }

        //Relation avec l'utilisateur, 1 NC peut avoir un utilisateur ManyToOne
        [Required]
        public string ApplicationAuthUserId { get; set; }        
        public ApplicationAuthUser ApplicationAuthUser { get; set; }

        [Required]
        public string EtatActionId { get; set; }
        [Display(Name = "Etat")]
        public EtatAction EtatAction { get; set; } 

    }
}
