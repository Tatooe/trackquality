﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TrackQuality.Models
{
    public class ActionNC
    {
        [Key]
        public int ActionId { get; set; }

        [Required(ErrorMessage = "Une description de l'action est obligatoire")]
        [DataType(DataType.Text)]
        [Display(Name = "Description")]
        public string DescriptionAction { get; set; }

        [Required(ErrorMessage = "La date est obligatoire.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime DateAction { get; set; }

        //Si Cloture == true visible dans table action archivée
        public bool Cloture { get; set; }

        [Required]
        [ForeignKey("AnalyseNC")]
        public int analyseNCId { get; set; }
        public  AnalyseNC analyseNC { get; set; }


    }
}

