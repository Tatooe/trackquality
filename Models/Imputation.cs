﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TrackQuality.Models
{

    public class Imputation
    {
        [Key]
        [Required(ErrorMessage = "Une imputation doit être renseignée.")]
        [Display(Name = "Imputation")]
        public string ImputationNC { get; set; }

        //Relation avec AnalyseNC, une imputation peut avoir plusieurs Analyse OneToMany
        public ICollection<AnalyseNC> AnalyseNCs { get; set; }

    }
}
