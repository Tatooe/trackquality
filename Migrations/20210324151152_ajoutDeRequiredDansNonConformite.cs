﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class ajoutDeRequiredDansNonConformite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EtatActionEtat",
                table: "NonConformite",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "EtatActionEtat",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
