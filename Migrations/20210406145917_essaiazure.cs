﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class essaiazure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "EtatActionId",
                table: "NonConformite",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ImputationId",
                table: "AnalyseNC",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "EtatActionId",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ImputationId",
                table: "AnalyseNC",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
