﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class correctionerreurnumNCincrementstringtoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "NumeroNCIncrement",
                table: "NonConformite",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "NumeroNCIncrement",
                table: "NonConformite",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
