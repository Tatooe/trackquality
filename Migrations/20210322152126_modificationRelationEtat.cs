﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class modificationRelationEtat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_AnalyseNC_AnalyseId",
                table: "ActionNC");

            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_EtatAction_EtatId",
                table: "ActionNC");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_AnalyseId",
                table: "ActionNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_EtatId",
                table: "ActionNC");

            migrationBuilder.DropColumn(
                name: "EtatId",
                table: "EtatAction");

            migrationBuilder.DropColumn(
                name: "AnalyseId",
                table: "ActionNC");

            migrationBuilder.DropColumn(
                name: "EtatId",
                table: "ActionNC");

            migrationBuilder.AddColumn<int>(
                name: "EtatActionId",
                table: "NonConformite",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EtatActionId",
                table: "EtatAction",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ActionId",
                table: "AnalyseNC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction",
                column: "EtatActionId");

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ActionId",
                table: "AnalyseNC",
                column: "ActionId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC",
                column: "NonConformiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_ActionNC_ActionId",
                table: "AnalyseNC",
                column: "ActionId",
                principalTable: "ActionNC",
                principalColumn: "ActionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId",
                principalTable: "EtatAction",
                principalColumn: "EtatActionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_ActionNC_ActionId",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ActionId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropColumn(
                name: "EtatActionId",
                table: "EtatAction");

            migrationBuilder.DropColumn(
                name: "ActionId",
                table: "AnalyseNC");

            migrationBuilder.AddColumn<int>(
                name: "EtatId",
                table: "EtatAction",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "AnalyseId",
                table: "ActionNC",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EtatId",
                table: "ActionNC",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction",
                column: "EtatId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC",
                column: "NonConformiteId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_AnalyseId",
                table: "ActionNC",
                column: "AnalyseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_EtatId",
                table: "ActionNC",
                column: "EtatId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_AnalyseNC_AnalyseId",
                table: "ActionNC",
                column: "AnalyseId",
                principalTable: "AnalyseNC",
                principalColumn: "AnalyseId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_EtatAction_EtatId",
                table: "ActionNC",
                column: "EtatId",
                principalTable: "EtatAction",
                principalColumn: "EtatId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
