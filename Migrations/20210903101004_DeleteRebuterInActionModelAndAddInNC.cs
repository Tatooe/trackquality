﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class DeleteRebuterInActionModelAndAddInNC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rebuter",
                table: "ActionNC");

            migrationBuilder.AddColumn<bool>(
                name: "Rebuter",
                table: "NonConformite",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rebuter",
                table: "NonConformite");

            migrationBuilder.AddColumn<bool>(
                name: "Rebuter",
                table: "ActionNC",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
