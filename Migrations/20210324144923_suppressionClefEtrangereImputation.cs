﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class suppressionClefEtrangereImputation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "ImputationId",
                table: "Imputation");

            migrationBuilder.DropColumn(
                name: "ImputationId",
                table: "AnalyseNC");

            migrationBuilder.AlterColumn<string>(
                name: "ImputationNC",
                table: "Imputation",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "ImputationNC",
                table: "AnalyseNC",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation",
                column: "ImputationNC");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationNC",
                table: "AnalyseNC",
                column: "ImputationNC");

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC",
                column: "ImputationNC",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.AlterColumn<int>(
                name: "ImputationNC",
                table: "Imputation",
                type: "int",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "ImputationId",
                table: "Imputation",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ImputationId",
                table: "AnalyseNC",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation",
                column: "ImputationId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId");

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId",
                principalTable: "Imputation",
                principalColumn: "ImputationId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
