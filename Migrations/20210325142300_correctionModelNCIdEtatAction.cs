﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class correctionModelNCIdEtatAction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.DropColumn(
                name: "EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.AddColumn<string>(
                name: "EtatActionId",
                table: "NonConformite",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropColumn(
                name: "EtatActionId",
                table: "NonConformite");

            migrationBuilder.AddColumn<string>(
                name: "EtatActionEtat",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
