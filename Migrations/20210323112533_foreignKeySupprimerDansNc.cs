﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class foreignKeySupprimerDansNc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_UserId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_UserId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropColumn(
                name: "ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_UserId",
                table: "NonConformite",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_UserId",
                table: "NonConformite",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
