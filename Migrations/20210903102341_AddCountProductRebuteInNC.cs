﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class AddCountProductRebuteInNC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "QtRebute",
                table: "NonConformite",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "QtRebute",
                table: "NonConformite");
        }
    }
}
