﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class majUserIdDansNC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
