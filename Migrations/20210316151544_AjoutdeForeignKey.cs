﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class AjoutdeForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_AnalyseNC_AnalyseNCAnalyseId",
                table: "ActionNC");

            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_EtatAction_EtatActionEtatId",
                table: "ActionNC");

            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationMImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ImputationMImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_AnalyseNCAnalyseId",
                table: "ActionNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_EtatActionEtatId",
                table: "ActionNC");

            migrationBuilder.DropColumn(
                name: "ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.DropColumn(
                name: "MImputationId",
                table: "Imputation");

            migrationBuilder.DropColumn(
                name: "ImputationMImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "AnalyseNCAnalyseId",
                table: "ActionNC");

            migrationBuilder.DropColumn(
                name: "EtatActionEtatId",
                table: "ActionNC");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ImputationId",
                table: "Imputation",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ImputationId",
                table: "AnalyseNC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EtatId",
                table: "ActionNC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation",
                column: "ImputationId");

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_UserId",
                table: "NonConformite",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC",
                column: "NonConformiteId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_AnalyseId",
                table: "ActionNC",
                column: "AnalyseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_EtatId",
                table: "ActionNC",
                column: "EtatId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_AnalyseNC_AnalyseId",
                table: "ActionNC",
                column: "AnalyseId",
                principalTable: "AnalyseNC",
                principalColumn: "AnalyseId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_EtatAction_EtatId",
                table: "ActionNC",
                column: "EtatId",
                principalTable: "EtatAction",
                principalColumn: "EtatId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId",
                principalTable: "Imputation",
                principalColumn: "ImputationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_UserId",
                table: "NonConformite",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_AnalyseNC_AnalyseId",
                table: "ActionNC");

            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_EtatAction_EtatId",
                table: "ActionNC");

            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_UserId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_UserId",
                table: "NonConformite");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_AnalyseId",
                table: "ActionNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_EtatId",
                table: "ActionNC");

            migrationBuilder.DropColumn(
                name: "ImputationId",
                table: "Imputation");

            migrationBuilder.DropColumn(
                name: "ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "EtatId",
                table: "ActionNC");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MImputationId",
                table: "Imputation",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ImputationMImputationId",
                table: "AnalyseNC",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AnalyseNCAnalyseId",
                table: "ActionNC",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EtatActionEtatId",
                table: "ActionNC",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Imputation",
                table: "Imputation",
                column: "MImputationId");

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationMImputationId",
                table: "AnalyseNC",
                column: "ImputationMImputationId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC",
                column: "NonConformiteId");

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_AnalyseNCAnalyseId",
                table: "ActionNC",
                column: "AnalyseNCAnalyseId");

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_EtatActionEtatId",
                table: "ActionNC",
                column: "EtatActionEtatId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_AnalyseNC_AnalyseNCAnalyseId",
                table: "ActionNC",
                column: "AnalyseNCAnalyseId",
                principalTable: "AnalyseNC",
                principalColumn: "AnalyseId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_EtatAction_EtatActionEtatId",
                table: "ActionNC",
                column: "EtatActionEtatId",
                principalTable: "EtatAction",
                principalColumn: "EtatId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationMImputationId",
                table: "AnalyseNC",
                column: "ImputationMImputationId",
                principalTable: "Imputation",
                principalColumn: "MImputationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
