﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class clefetrangereEtatActionSupprime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction");

            migrationBuilder.DropColumn(
                name: "EtatActionId",
                table: "NonConformite");

            migrationBuilder.DropColumn(
                name: "EtatActionId",
                table: "EtatAction");

            migrationBuilder.AddColumn<string>(
                name: "EtatActionEtat",
                table: "NonConformite",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Etat",
                table: "EtatAction",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction",
                column: "Etat");

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.DropIndex(
                name: "IX_NonConformite_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction");

            migrationBuilder.DropColumn(
                name: "EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.AddColumn<int>(
                name: "EtatActionId",
                table: "NonConformite",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Etat",
                table: "EtatAction",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "EtatActionId",
                table: "EtatAction",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EtatAction",
                table: "EtatAction",
                column: "EtatActionId");

            migrationBuilder.CreateIndex(
                name: "IX_NonConformite_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionId",
                table: "NonConformite",
                column: "EtatActionId",
                principalTable: "EtatAction",
                principalColumn: "EtatActionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
