﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class majrelationdansnc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "EtatActionEtat",
                table: "NonConformite",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "NonConformite",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EtatActionEtat",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_EtatAction_EtatActionEtat",
                table: "NonConformite",
                column: "EtatActionEtat",
                principalTable: "EtatAction",
                principalColumn: "Etat",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
