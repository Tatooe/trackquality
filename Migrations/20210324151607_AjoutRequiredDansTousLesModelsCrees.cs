﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class AjoutRequiredDansTousLesModelsCrees : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "Photo",
                table: "NonConformite",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ImputationNC",
                table: "AnalyseNC",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC",
                column: "ImputationNC",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite");

            migrationBuilder.AlterColumn<string>(
                name: "Photo",
                table: "NonConformite",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 250);

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationAuthUserId",
                table: "NonConformite",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ImputationNC",
                table: "AnalyseNC",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC",
                column: "ImputationNC",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NonConformite_AspNetUsers_ApplicationAuthUserId",
                table: "NonConformite",
                column: "ApplicationAuthUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
