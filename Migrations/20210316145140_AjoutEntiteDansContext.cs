﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class AjoutEntiteDansContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EtatAction",
                columns: table => new
                {
                    EtatId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Etat = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EtatAction", x => x.EtatId);
                });

            migrationBuilder.CreateTable(
                name: "Imputation",
                columns: table => new
                {
                    MImputationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ImputationNC = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imputation", x => x.MImputationId);
                });

            migrationBuilder.CreateTable(
                name: "AnalyseNC",
                columns: table => new
                {
                    AnalyseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MainDOeuvre = table.Column<string>(nullable: false),
                    Milieu = table.Column<string>(nullable: false),
                    Matiere = table.Column<string>(nullable: false),
                    Methode = table.Column<string>(nullable: false),
                    Materiel = table.Column<string>(nullable: false),
                    NonConformiteId = table.Column<int>(nullable: false),
                    ImputationMImputationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnalyseNC", x => x.AnalyseId);
                    table.ForeignKey(
                        name: "FK_AnalyseNC_Imputation_ImputationMImputationId",
                        column: x => x.ImputationMImputationId,
                        principalTable: "Imputation",
                        principalColumn: "MImputationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AnalyseNC_NonConformite_NonConformiteId",
                        column: x => x.NonConformiteId,
                        principalTable: "NonConformite",
                        principalColumn: "NonConformiteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActionNC",
                columns: table => new
                {
                    ActionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DescriptionAction = table.Column<string>(nullable: true),
                    DateAction = table.Column<DateTime>(nullable: false),
                    AnalyseId = table.Column<int>(nullable: false),
                    AnalyseNCAnalyseId = table.Column<int>(nullable: true),
                    EtatActionEtatId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionNC", x => x.ActionId);
                    table.ForeignKey(
                        name: "FK_ActionNC_AnalyseNC_AnalyseNCAnalyseId",
                        column: x => x.AnalyseNCAnalyseId,
                        principalTable: "AnalyseNC",
                        principalColumn: "AnalyseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActionNC_EtatAction_EtatActionEtatId",
                        column: x => x.EtatActionEtatId,
                        principalTable: "EtatAction",
                        principalColumn: "EtatId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_AnalyseNCAnalyseId",
                table: "ActionNC",
                column: "AnalyseNCAnalyseId");

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_EtatActionEtatId",
                table: "ActionNC",
                column: "EtatActionEtatId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationMImputationId",
                table: "AnalyseNC",
                column: "ImputationMImputationId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_NonConformiteId",
                table: "AnalyseNC",
                column: "NonConformiteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActionNC");

            migrationBuilder.DropTable(
                name: "AnalyseNC");

            migrationBuilder.DropTable(
                name: "EtatAction");

            migrationBuilder.DropTable(
                name: "Imputation");
        }
    }
}
