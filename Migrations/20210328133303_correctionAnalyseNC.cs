﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class correctionAnalyseNC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_ActionNC_ActionId",
                table: "AnalyseNC");

            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ActionId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "ActionId",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "ImputationNC",
                table: "AnalyseNC");

            migrationBuilder.AlterColumn<string>(
                name: "PhotoNC",
                table: "NonConformite",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250);

            migrationBuilder.AddColumn<string>(
                name: "ImputationId",
                table: "AnalyseNC",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId");

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC",
                column: "ImputationId",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropIndex(
                name: "IX_AnalyseNC_ImputationId",
                table: "AnalyseNC");

            migrationBuilder.DropColumn(
                name: "ImputationId",
                table: "AnalyseNC");

            migrationBuilder.AlterColumn<string>(
                name: "PhotoNC",
                table: "NonConformite",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ActionId",
                table: "AnalyseNC",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ImputationNC",
                table: "AnalyseNC",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ActionId",
                table: "AnalyseNC",
                column: "ActionId");

            migrationBuilder.CreateIndex(
                name: "IX_AnalyseNC_ImputationNC",
                table: "AnalyseNC",
                column: "ImputationNC");

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_ActionNC_ActionId",
                table: "AnalyseNC",
                column: "ActionId",
                principalTable: "ActionNC",
                principalColumn: "ActionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AnalyseNC_Imputation_ImputationNC",
                table: "AnalyseNC",
                column: "ImputationNC",
                principalTable: "Imputation",
                principalColumn: "ImputationNC",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
