﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class relationActionVSAnalyse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "analyseNCId",
                table: "ActionNC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ActionNC_analyseNCId",
                table: "ActionNC",
                column: "analyseNCId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActionNC_AnalyseNC_analyseNCId",
                table: "ActionNC",
                column: "analyseNCId",
                principalTable: "AnalyseNC",
                principalColumn: "AnalyseId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActionNC_AnalyseNC_analyseNCId",
                table: "ActionNC");

            migrationBuilder.DropIndex(
                name: "IX_ActionNC_analyseNCId",
                table: "ActionNC");

            migrationBuilder.DropColumn(
                name: "analyseNCId",
                table: "ActionNC");
        }
    }
}
