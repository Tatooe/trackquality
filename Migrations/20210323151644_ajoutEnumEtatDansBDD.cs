﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrackQuality.Migrations
{
    public partial class ajoutEnumEtatDansBDD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Etat",
                table: "EtatAction",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Etat",
                table: "EtatAction",
                type: "int",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
