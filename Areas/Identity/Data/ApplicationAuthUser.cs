﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using TrackQuality.Models;

namespace TrackQuality.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the ApplicationAuthUser class
    public class ApplicationAuthUser : IdentityUser
    {

        //Ajout personnalisation de la table IdentityUser avec validation des données et message d'erreur
        [PersonalData]
        [Required]
        [RegularExpression(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", ErrorMessage = "Votre {0} ne doit contenir pas de caractères spéciaux")]
        [StringLength(50, ErrorMessage = "Le nombre de caractères de votre {0} est limité à {1}.")]
        public string Nom { get; set; }

        [PersonalData]
        [Required]
        [RegularExpression(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", ErrorMessage = "Votre {0} ne doit contenir pas de caractères spéciaux")]
        [StringLength(50, ErrorMessage = "Le nombre de caractères de votre {0} est limité à {1}.")]
        [Display(Name = "Prénom")]
        public string Prenom { get; set; }        

        //Relation avec les NonConformités OneToMany: Un utilisateur peut avoir plusieurs NC
        public ICollection<NonConformite> NonConformites { get; set; }
       

    }
}
