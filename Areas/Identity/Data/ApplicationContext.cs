﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Models;

namespace TrackQuality.Data
{
    //Classe principale qui coordonner la fonctionnalité d'EntityFramework
    public class ApplicationContext : IdentityDbContext<ApplicationAuthUser, IdentityRole, string>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }
        public DbSet<Produit> Produit { get; set; }
        public DbSet<NonConformite> NonConformite { get; set; }
        public DbSet<AnalyseNC> AnalyseNC { get; set; }
        public DbSet<Imputation> Imputation { get; set; }
        public DbSet<ActionNC> ActionNC { get; set; }
        public DbSet<EtatAction> EtatAction { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

    }
}
