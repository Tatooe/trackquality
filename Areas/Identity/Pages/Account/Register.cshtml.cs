﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using TrackQuality.Areas.Identity.Data;

namespace TrackQuality.Areas.Identity.Pages.Account
{
    //Classe gérant l'enregistrement de nouvel utilisateur
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationAuthUser> _signInManager;
        private readonly UserManager<ApplicationAuthUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;

        //contructeur
        public RegisterModel(
            UserManager<ApplicationAuthUser> userManager,
            SignInManager<ApplicationAuthUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }
        
        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            

            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [RegularExpression(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", ErrorMessage = "Votre {0} ne doit contenir pas de caractères spéciaux")]
            [StringLength(50, ErrorMessage = "Le nombre de caractères de votre {0} est limité à {1}.")]
            public string Nom { get; set; }


            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [RegularExpression(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", ErrorMessage = "Votre {0} ne doit contenir pas de caractères spéciaux")]
            [StringLength(50, ErrorMessage = "Le nombre de caractères de votre {0} est limité à {1}.")]
            [Display(Name = "Prénom")]
            public string Prenom { get; set; }

            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [EmailAddress(ErrorMessage =" Votre {0} n'est pas valide")]
            [Display(Name = "Email")]
            public string Email { get; set; }


            [Required(ErrorMessage = "Le champ {0} est obligatoire")]
            [StringLength(100, ErrorMessage = "Le {0} doit être au minimum de {2} et au maximum {1} caractères de longueur.", MinimumLength = 6)]
            [RegularExpression(@"^(?=.*[a-z])(?=.*\d)(?=.*[-+!*/$@%_])([-+!*/$@%_\w]{6,100})$",ErrorMessage ="Votre {0} doit avoir au moins une lettre minuscule,un chiffre, un caractère spécial ($ @ % * + - _ ou !)")]
            [DataType(DataType.Password)]
            [Display(Name = "Mot de passe")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirmer le mot de passe")]
            [Compare("Password", ErrorMessage = "Les mots de passe doivent être identiques.")]
            public string ConfirmPassword { get; set; }
        }

        //Méthode pour vérifier si le mail existe renvoie un booléén
        private bool MailExists(string id)
        {
            return _userManager.Users.Any(e => e.Email == id);
        }
        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            if (ModelState.IsValid)
            {
                //Si le formulaire est valide on instancie une nouvelle classe ApplicationAuthUser avec les informations renseignés par l'utilisateur
                var user = new ApplicationAuthUser { UserName = Input.Email, Email = Input.Email, Nom = Input.Nom, Prenom = Input.Prenom };
                //On enregistre le mot de passe chiffré
                var result = await _userManager.CreateAsync(user, Input.Password);

                 if (MailExists(Input.Email))
                {
                    ModelState.AddModelError(string.Empty, "Vous êtes déjà enregistrer avec cet identifiant.");
                }

                if (result.Succeeded)
                {
                    //SI OK information à la console
                    _logger.LogInformation("L'utilisateur à créer un nouveau compte avec un mot de passe");

                    //Génération d'un token pour la confirmation d'email
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //Le token est chiffré
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    //Création d'une url pour la validation de l'adresse mail
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    //envoie un mail à la personne qui s'inscrit
                    await _emailSender.SendEmailAsync(Input.Email, "Confirmer votre adresse email",
                        $"Svp, merci de confirmer votre compte <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>cliquer ici</a>.");

                    IList<ApplicationAuthUser> AllAdmin = await _userManager.GetUsersInRoleAsync("Admin");

                    //Envoie un mail à tous les admins pour donner des droits d'accès à l'utilisateur inscrit pour l'alerter
                    for(int i = 0; i < AllAdmin.Count; i++)
                    {
                        await _emailSender.SendEmailAsync(AllAdmin[i].Email, "Nouvel utilisateur inscrit",
                         $"Svp, une personne s'est inscrite. Merci de donner un rôle au nouvel utilisateur.");

                    }

                    //Redirection si la confirmation de l'email est bonne
                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {                        
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });                        
                    }                   
                    else
                    {
                        //Si ko redirection vers page d'accueil
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }

                }

                foreach (var error in result.Errors)
                {
                    //Gestion des erreurs
                    ModelState.AddModelError( string.Empty, "Une erreur s'est produite merci de réessayer plus tard.");
                }
            }

            
            // Si quelque chose se passe mal retourne à la page d'inscription
            return Page();
        }
    }
}
