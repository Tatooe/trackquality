﻿using System;
using AspNetCoreEmailConfirmationSendGrid.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Data;


//Classe de démarrage de l'application avec tous les services utiles à l'application
[assembly: HostingStartup(typeof(TrackQuality.Areas.Identity.IdentityHostingStartup))]
namespace TrackQuality.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            //Connection à la base de données et appelle du contexte utilisé au démarrage
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<ApplicationContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("ApplicationContextConnection")));

                //Services utilisés du package identity
                services.AddDefaultIdentity<ApplicationAuthUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<ApplicationContext>()
                    .AddDefaultTokenProviders();
                //services sendgrid pour l'envoi de mail
                services.AddTransient<IEmailSender, EmailSender>();
                //Configuration des caractères requis pour le mot de passe
                services.Configure<IdentityOptions>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 1;                   

                });           

                

            });
        }
    }
}