﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TrackQuality.Data;
using TrackQuality.Models;

namespace TrackQuality.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationContext context)
        {
            _logger = logger;
            _context = context;
        }

        int currentYear = DateTime.Now.Year;

        //Récupération de tous les numéros NC sur l'année en cours
        private IQueryable<string> GetNumNCByCurrentYear() => from item in _context.NonConformite where item.DateNC.Year == currentYear select item.NumeroNC;
        //Récupération de tous les produits NC sur l'année en cours
        private IQueryable<int> GetQtProduitNCByCurrentYear() => from item in _context.NonConformite where item.DateNC.Year == currentYear select item.QuantiteProdNC;
        //Récupération de tous les coûts de NC sur l'année en cours
        private IQueryable<double> GetCoutNCByCurrentYear() => from item in _context.NonConformite where item.DateNC.Year == currentYear select item.Produit.Prix;
        //Récupération de toutes les références de produits NC sur l'année en cours
        private IQueryable<string> GetRefProdNCByCurrentYear() => from item in _context.NonConformite where item.DateNC.Year == currentYear select item.Produit.Reference;
        //Récupération de toutes les responsabilité de NC sur l'année en cours
        private IQueryable<string> GetResponsabititeNCByCurrentYear() => from item in _context.AnalyseNC where item.DateAnalyse.Year == currentYear select item.Imputation.ImputationNC;
        //Récupération de la quantité de produit Rebuter
        private IQueryable<int> GetCountOfTrashProduct() => from item in _context.NonConformite select item.QtRebute;
        public IActionResult Index()
        {
            
            ViewData["TotalNC"] = GetNumNCByCurrentYear().Count();
            ViewData["NbProduitsNC"] = GetQtProduitNCByCurrentYear().Sum();
            ViewData["CoutNC"] = GetCoutNCByCurrentYear().Sum();
            ViewData["TopProduitNC"] = GetRefProdNCByCurrentYear().Max();
            ViewData["TopResponsabiliteNC"] = GetResponsabititeNCByCurrentYear().Max();
            ViewData["NbProduitRebute"] = GetCountOfTrashProduct().Sum();

            return View();
        }

        public IActionResult GetDataNCLine()
        {
            var currentYear = DateTime.Now.Year;
            var nbNcByMonthQuery = _context.NonConformite
                .Where(p => p.DateNC.Year == currentYear)
                .GroupBy(p => p.DateNC.Month)
                .Select(p => new { name = p.Key, count = p.Count() });
            return Json(nbNcByMonthQuery);
        }

        public IActionResult GetDataProduitNCDonut()
        {
            var currentYear = DateTime.Now.Year;
            var produitNcDonutQuery = _context.NonConformite.Where(p => p.DateNC.Year == currentYear).GroupBy(p => p.Produit.Reference).Select(p => new { name = p.Key, count = p.Sum(x => x.QuantiteProdNC) });

                return Json(produitNcDonutQuery);
        }

        public IActionResult GetDataResponsabilitePie()
        {
            var currentYear = DateTime.Now.Year;
            var responsabilteQuery = _context.AnalyseNC.Include("NonConformite").Where(p => p.NonConformite.DateNC.Year == currentYear).GroupBy(p => p.ImputationId).Select(p => new { name = p.Key, count = p.Count() });

            return Json(responsabilteQuery);
        }
        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
