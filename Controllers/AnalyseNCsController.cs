﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Data;
using TrackQuality.Models;

namespace TrackQuality.Controllers
{
    public class AnalyseNCsController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly UserManager<ApplicationAuthUser> _userManager;

        public AnalyseNCsController(ApplicationContext context, UserManager<ApplicationAuthUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }


        //Requête pour récupérer une nonconformite par Id en passant par NonConformite
        private IQueryable<NonConformite> ReqFullNcByIdNcFromNc(int? idNc) => from item in _context.NonConformite where item.NonConformiteId == idNc select item;
       //Requête pour récupérer l'id d'une NC par l'entité analyse
        private IQueryable<int> ReqIdNcFromAnalyse(int? id) => from item in _context.AnalyseNC where item.AnalyseId == id select item.NonConformiteId;
       //Requête pour récupére la référence (nom) d'un produit par son Id en passant par analyseNC
        private IQueryable<string> ReqRefProdByProduitId(int? prodId) => from item in _context.Produit where item.ProduitId == prodId select item.Reference;
        //Requête pour récupérer tous les états disponible de la base de données EtatAction
        private IQueryable<String> ReqFullEtatNcByEtatAction() => from item in _context.EtatAction select item.Etat;
        //Requête pour récupérer les informations de la personne ayant ouvert la NC
        private IQueryable<ApplicationAuthUser> ReqPersCreateNC(string userId) => from item in _userManager.Users where item.Id == userId select item;
        

        private void NonConformiteViewByIdNc(int? idNc)
        {
            //Requête pour récupérer les informations de la NC (suivant l'id) à traiter et les afficher
            var nonConformites = ReqFullNcByIdNcFromNc(idNc).FirstOrDefault();
            ViewData["NumeroNC"] = nonConformites.NumeroNC;
            ViewData["NumeroSerie"] = nonConformites.NumeroSerie;
            ViewData["Quantite"] = nonConformites.QuantiteProdNC;
            ViewData["DescriptionNC"] = nonConformites.DescriptionNC;
            ViewData["Photo"] = nonConformites.PhotoNC;
            ViewData["Nom"] = ReqPersCreateNC(nonConformites.ApplicationAuthUserId).FirstOrDefault().Nom;
            ViewData["Prenom"] = ReqPersCreateNC(nonConformites.ApplicationAuthUserId).FirstOrDefault().Prenom;
            ViewData["DateNC"] = nonConformites.DateNC;
            int prodId = nonConformites.ProduitId;
            ViewData["Produit"] = ReqRefProdByProduitId(prodId).FirstOrDefault();

            //Création de la vue pour créer une select list avec le nom de l'imputation
            ViewData["ImputationId"] = new SelectList(_context.Set<Imputation>(), "ImputationNC", "ImputationNC");
        }


        private void NonConformiteViewByIdAnalyse(int? id)
        {
            var idNC = ReqIdNcFromAnalyse(id).FirstOrDefault();
            //Requête pour récupérer les informations de la NC (suivant l'id) à traiter et les afficher
            var nonConformites = ReqFullNcByIdNcFromNc(idNC).FirstOrDefault();
            ViewData["NumeroNC"] = nonConformites.NumeroNC;
            ViewData["NumeroSerie"] = nonConformites.NumeroSerie;
            ViewData["Quantite"] = nonConformites.QuantiteProdNC;
            ViewData["DescriptionNC"] = nonConformites.DescriptionNC;
            ViewData["Photo"] = nonConformites.PhotoNC;
            ViewData["Nom"] = ReqPersCreateNC(nonConformites.ApplicationAuthUserId).FirstOrDefault().Nom;
            ViewData["Prenom"] = ReqPersCreateNC(nonConformites.ApplicationAuthUserId).FirstOrDefault().Prenom;
            ViewData["DateNC"] = nonConformites.DateNC;
            int prodId = nonConformites.ProduitId;
            ViewData["Produit"] = ReqRefProdByProduitId(prodId).FirstOrDefault();

            //Création de la vue pour créer une select list avec le nom de l'imputation
            ViewData["ImputationId"] = new SelectList(_context.Set<Imputation>(), "ImputationNC", "ImputationNC");
        }


        private bool AnalyseNCExists(int id)
        {
            return _context.AnalyseNC.Any(e => e.AnalyseId == id);
        }

        // GET: AnalyseNCs/Details/5
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var analyseNC = await _context.AnalyseNC
                .Include(a => a.Imputation)
                .Include(a => a.NonConformite)
                .FirstOrDefaultAsync(m => m.AnalyseId == id);
            if (analyseNC == null)
            {
                return NotFound();
            }
            //Requête pour récupérer les informations de la NC (suivant l'id) à traiter et les afficher
            var idNc = ReqIdNcFromAnalyse(id).FirstOrDefault();
            NonConformiteViewByIdNc(idNc);                     

            return View(analyseNC);
        }
                
        // GET: AnalyseNCs/Create
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            NonConformiteViewByIdNc(id);

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Create(int id, [Bind("AnalyseId,DateAnalyse,MainDOeuvre,Milieu,Matiere,Methode,Materiel,NonConformiteId,ImputationId")] AnalyseNC analyseNC)
        {


            var nonconformite = await _context.NonConformite.Where(x => x.NonConformiteId == id).SingleOrDefaultAsync();
            //Génération de l'état en cours par défaut avec requête linq dans bdd etataction 
            var etat = ReqFullEtatNcByEtatAction().ToArray();
            nonconformite.EtatActionId = etat[1];

            //Récupération de l'id de la NonConformité et ajout dans table Analyse
            analyseNC.NonConformiteId = id;
            //Génération de la date du moment pour la création des NC
            DateTime dateAnalyse = DateTime.Now;
            analyseNC.DateAnalyse = dateAnalyse;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nonconformite);
                    _context.Add(analyseNC);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "TimeLine", new { id = id });
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                }

            }

            NonConformiteViewByIdNc(id);

            //Création de la vue pour créer une select list avec le nom de l'imputation et récupérer l'id pour l'intégrer dans la table analyse
            ViewData["ImputationId"] = new SelectList(_context.Set<Imputation>(), "ImputationNC", "ImputationNC", analyseNC.ImputationId);

            return View(analyseNC);
        }



        //GET: AnalyseNCs/Edit/5
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var analyseNC = await _context.AnalyseNC.FindAsync(id);
            if (analyseNC == null)
            {
                return NotFound();
            }
            //Préparation de la vue avec les infos de la nonconformité
            NonConformiteViewByIdAnalyse(id);
            ViewData["ImputationId"] = new SelectList(_context.Set<Imputation>(), "ImputationNC", "ImputationNC", analyseNC.ImputationId);
            return View(analyseNC);
        }


        //POST: AnalyseNCs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Edit(int id, [Bind("AnalyseId,MainDOeuvre,Milieu,Matiere,Methode,Materiel,NonConformiteId,ImputationId,DateAnalyse")] AnalyseNC analyseNC)
        {
            if (id != analyseNC.AnalyseId)
            {
                return NotFound();
            }

            var idNc = ReqIdNcFromAnalyse(id).FirstOrDefault();

            analyseNC.NonConformiteId = ReqIdNcFromAnalyse(id).FirstOrDefault();

            var nonconformite = await _context.NonConformite.Where(x => x.NonConformiteId == ReqIdNcFromAnalyse(id).FirstOrDefault()).SingleOrDefaultAsync();
            //Génération de l'état en cours par défaut avec requête linq dans bdd etataction 
            nonconformite.EtatActionId = ReqFullEtatNcByEtatAction().ToArray()[1];

            //Génération de la date du moment pour la création des NC
            analyseNC.DateAnalyse = DateTime.Now;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nonconformite);
                    _context.Update(analyseNC);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnalyseNCExists(analyseNC.AnalyseId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "TimeLine", new { id = idNc });
            }

            NonConformiteViewByIdAnalyse(id);

            return View(analyseNC);
        }


    }
}
