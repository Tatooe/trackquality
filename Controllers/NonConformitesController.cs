﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Data;
using TrackQuality.Models;
using TrackQuality.ViewModels;

namespace TrackQuality.Controllers
{
    public class NonConformitesController : Controller
    {
        //Injection de dépendances
        private readonly ApplicationContext _context;
        private readonly UserManager<ApplicationAuthUser> _userManager;
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly RoleManager<IdentityRole> _roleManager;

        //Demande de paramètres dans le constructeur pour pouvoir les utiliser dans les méthodes
        public NonConformitesController(ApplicationContext context, UserManager<ApplicationAuthUser> userManager, IWebHostEnvironment appEnvironment, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _appEnvironment = appEnvironment;
            _roleManager = roleManager;
        }

        //Requêtes en linq
        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite
        private IQueryable<String> ReqFullNcByNc()=> from item in _context.NonConformite select item.NumeroNC;

        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite et trié par EtatAction "En cours"
        private IQueryable<String> ReqFullNcByNcByEnCours() => from item in _context.NonConformite where item.EtatActionId == "En Cours" select item.EtatActionId;

        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite et trié par EtatAction "A Traiter"
        private IQueryable<String> ReqFullNcByNcByATraiter() => from item in _context.NonConformite where item.EtatActionId == "A Traiter" select item.EtatActionId;

        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite et trié par EtatAction "Fermer"
        private IQueryable<String> ReqFullNcByNcByFermer() => from item in _context.NonConformite where item.EtatActionId == "Fermer" select item.EtatActionId;

        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite et trié par EtatAction "En cours" et trié par utilisateur connecté
        private IQueryable<String> ReqFullNcByNcByEnCoursByUser() => from item in _context.NonConformite where item.EtatActionId == "En Cours" where item.ApplicationAuthUserId == _userManager.GetUserId(User) select item.EtatActionId;

        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite et trié par EtatAction "A Traiter" et trié par utilisateur connecté
        private IQueryable<String> ReqFullNcByNcByATraiterByUser() => from item in _context.NonConformite where item.EtatActionId == "A Traiter" where item.ApplicationAuthUserId == _userManager.GetUserId(User) select item.EtatActionId;

        //Requête pour récupérer toutes les non-conformités en passant par le modèle NonConformite et trié par EtatAction "A Traiter" et trié par utilisateur connecté
        private IQueryable<String> ReqFullNcByNcByFermerByUser() => from item in _context.NonConformite where item.EtatActionId == "Fermer" where item.ApplicationAuthUserId == _userManager.GetUserId(User) select item.EtatActionId;

        //Requête pour récupérer toutes les numeroNC en passant par le modèle NonConformite  trié par utilisateur connecté
        private IQueryable<String> ReqFullNumNcByNcByUser() => from item in _context.NonConformite where item.ApplicationAuthUserId == _userManager.GetUserId(User) select item.NumeroNC;

        //Requête pour récupérer tous les états disponible dans le modèle EtatAction
        private IQueryable<String> ReqFullEtatNcByEtatAction() => from item in _context.EtatAction select item.Etat;

        //Requête pour récupérer la photo par NonConformiteId dans le modèle NonConformite
        private IQueryable<String> ReqPhotoByIdByNC(int id) => from item in _context.NonConformite where item.NonConformiteId == id select item.PhotoNC;

        //Requête pour récupérer tous les NumeroNCIncrement dans le modèle NonConformite
        private IQueryable<int> ReqNumIncrementByNC() => from item in _context.NonConformite select item.NumeroNCIncrement;




        //Méthode private qui renvoie un booléen true ou false pour voir si la non-confomité existe déjà en fonction de l'Id
        private bool NonConformiteExists(int id)
        {
            return _context.NonConformite.Any(e => e.NonConformiteId == id);
        }


        //Méthode public récupérant les informations dans le modèle NonConformite et donnant les informations à la vue
        //ViewData pour donner les datas à la vue partielle _IndexSommeNc.cshtml
        // GET: NonConformites
        [HttpGet]
        [Authorize(Roles ="Qualité")]
        public async Task<IActionResult> Index()
        {

            //var trackQualityContext = _context.NonConformite.Include(n => n.ApplicationAuthUser).Include(n => n.EtatAction).Include(n => n.Produit);

            AnalyseActionByNCViewModel model = new AnalyseActionByNCViewModel
            {
                NonConformites = await _context.NonConformite.Include(n => n.ApplicationAuthUser).Include(n => n.EtatAction).Include(n => n.Produit).ToListAsync(),
                AnalyseNCs = await _context.AnalyseNC.Include(n => n.Imputation).ToListAsync(),
                ActionNCs = await _context.ActionNC.Where(p => p.Cloture == false).ToListAsync()
            };

            ViewData["NbNcEnCours"] = ReqFullNcByNcByEnCours().Count();
                ViewData["NbNcATraiter"] = ReqFullNcByNcByATraiter().Count();
                ViewData["NbNcFermer"] = ReqFullNcByNcByFermer().Count();
                ViewData["NbNcTotal"] = ReqFullNcByNc().Count();


            //return View(await trackQualityContext.ToListAsync());
            return View(model);
        }


        //Méthode public récupérant les informations dans le modèle NonConformite et donnant les information à la vue trié par personne connectée
        //ViewData pour donner les datas à la vue partielle _IndexSommeNc.cshtml en fonction des utilisateurs connectés
        //GET: NonConfromites en triant par personne connectée
        [HttpGet]
        [Authorize(Roles = "Opérateur")]
        public async Task<IActionResult> IndexUser()
        {
            //var listNCUser = (from item in _context.NonConformite where item.ApplicationAuthUserId == _userManager.GetUserId(User) select item).Include(n => n.EtatAction).Include(n => n.ApplicationAuthUser).Include(n => n.Produit);

            AnalyseActionByNCViewModel analyseActionByNCViewModel = new AnalyseActionByNCViewModel();
            AnalyseActionByNCViewModel model = analyseActionByNCViewModel;

            model.NonConformites = await _context.NonConformite.Include(n => n.ApplicationAuthUser).Include(n => n.EtatAction).Include(n => n.Produit).Where(p => p.ApplicationAuthUserId == _userManager.GetUserId(User)).ToListAsync();
            model.AnalyseNCs = await _context.AnalyseNC.Include(n => n.Imputation).ToListAsync();
            model.ActionNCs = await _context.ActionNC.Where(p=>p.Cloture == false).ToListAsync();


            ViewData["NbNcEnCours"] = ReqFullNcByNcByEnCoursByUser().Count();
            ViewData["NbNcATraiter"] = ReqFullNcByNcByATraiterByUser().Count();
            ViewData["NbNcFermer"] = ReqFullNcByNcByFermerByUser().Count();
            ViewData["NbNcTotal"] = ReqFullNumNcByNcByUser().Count();
            return View(model);

        }

        //Méthode pour récupérer toutes les NCs clôturées
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Archive()
        {
            AnalyseActionByNCViewModel analyseActionByNCViewModel = new AnalyseActionByNCViewModel();
            AnalyseActionByNCViewModel model = analyseActionByNCViewModel;
            model.NonConformites = await _context.NonConformite.Include(n => n.ApplicationAuthUser).Include(n => n.EtatAction).Include(n => n.Produit).ToListAsync(); ;
            model.AnalyseNCs = await _context.AnalyseNC.Include(n => n.Imputation).ToListAsync();
            model.ActionNCs = await _context.ActionNC.Where(p => p.Cloture ==  true).ToListAsync();

            return View(model);
        }

        //Méthode pour récupérer toutes les NCs clôturées par utilisateur opérateur
        [HttpGet]
        [Authorize(Roles = "Opérateur")]
        public async Task<IActionResult> ArchiveUser()
        {
            var model = new AnalyseActionByNCViewModel
            {
                NonConformites = await _context.NonConformite.Include(n => n.ApplicationAuthUser).Include(n => n.EtatAction).Include(n => n.Produit).Where(p => p.ApplicationAuthUserId == _userManager.GetUserId(User)).ToListAsync(),
                AnalyseNCs = await _context.AnalyseNC.Include(n => n.Imputation).ToListAsync(),
                ActionNCs = await _context.ActionNC.Where(p => p.Cloture == true).ToListAsync()
            };

            return View(model);
        }

        //Méthode afin d'afficher les détails de la non-conformité sélectionnée par l'Id
        // GET: NonConformites/Details/5
        [HttpGet]
        [Authorize(Roles = "Opérateur,Qualité")]
        public async Task<IActionResult> Details(int? id)
        {
            //Gestion de l'erreur si l'id est null
            if (id == null)
            {
                return NotFound();
            }

            //récupération de toutes les informations de la non-confromité en incluant ApplicationAuthUser, EtatAction et Produit par rapport à l'id sélectionné
            var nonConformite = await _context.NonConformite
                .Include(n => n.ApplicationAuthUser)
                .Include(n => n.EtatAction)
                .Include(n => n.Produit)
                .FirstOrDefaultAsync(m => m.NonConformiteId == id);

            //Gestion d'erreur en cas de résultat null
            if (nonConformite == null)
            {
                return NotFound();
            }

            return View(nonConformite);
        }




        //Méthode pour générer la vue de création de non-conformité
        // GET: NonConformites/Create
        [HttpGet]
        [Authorize(Roles = "Opérateur,Qualité")]
        public IActionResult Create()
        {
            //ViewData permettant d'avoir une liste déroulante de choix de produit dans la vue
            ViewData["ProduitId"] = new SelectList(_context.Set<Produit>(), "ProduitId", "Reference");
            return View();
        }



        //Méthode pour créer une nouvelle non-conformité dans la table NonConformite
        // POST: NonConformites/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Opérateur,Qualité")]
        public async Task<IActionResult> Create([Bind("NonConformiteId,NumeroNC,NumeroNCIncrement,DescriptionNC,PhotoNC,DateNC,QuantiteProdNC,NumeroSerie,ProduitId,ApplicationAuthUserId,EtatActionId")] NonConformite nonConformite)
        {
            //Création des numéros de non conformité
            //Récupération de l'année en cours
            string year = DateTime.Now.Year.ToString();
            //Initialisation de la variable récupérant le numeroIncrement le plus grand
            int idMaxQuery;
            //Condition qui permet d'initialiser à 1 en cas de base de données vide sinon on prend le numero le plus grand et on incrémente +1
            if(ReqNumIncrementByNC().FirstOrDefault() == 0)
            {
                idMaxQuery = 1;
            }
            else
            {
                idMaxQuery = ReqNumIncrementByNC().Max() + 1;
            }


            //paramètres par défaut de l'objet nonconformite
            nonConformite.NumeroNC = ("ANC" + year + "-" + idMaxQuery).ToString(); 
            nonConformite.DateNC = DateTime.Now;
            nonConformite.EtatActionId = (ReqFullEtatNcByEtatAction().ToArray())[0];
            nonConformite.ApplicationAuthUserId = _userManager.GetUserId(User);
            nonConformite.NumeroNCIncrement = idMaxQuery;
            nonConformite.PhotoNC = "noImage.jpg";

            //ViewData permettant d'avoir une liste déroulante de choix de produit dans la vue et d'enregistrer le choix sélectionné
            ViewData["ProduitId"] = new SelectList(_context.Set<Produit>(), "ProduitId", "Reference", nonConformite.ProduitId);


            //Récupération de la photo entrée par l'utilisateur 
            var files = HttpContext.Request.Form.Files;

            //Parcours de la variable files
            foreach (var PhotoNC in files)
            {
                //Si une photo est présente
                if (PhotoNC != null && PhotoNC.Length > 0)
                {
                    var file = PhotoNC;
                    //Création du chemin de l'Enregistrement dans le dossier wwwroot/images
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "images");
                    if (file.Length > 0)
                    {
                        //renommage de la photo pour avoir un nom unique en utilisant Guid qui génère des numéros uniques
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);

                        using var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create);
                        //Copie de la photo en local dans le dossier wwwroot/images
                        await file.CopyToAsync(fileStream);
                        //préparation à l'enregistrement dans le modèle NonConformite pour PhotoNC
                        nonConformite.PhotoNC = fileName;
                    }
                 }
            }

            //Si les informations sont correct et le formulaire est valide
            if (ModelState.IsValid)
            {
               
                try {
                    //essai d'ajout de tous les champs dans la table NonConformite
                    _context.Add(nonConformite);
                     await _context.SaveChangesAsync();

                    //Condition de redirection suivant le rôle de l'utilisateur connecté
                    if (User.IsInRole("Qualité"))
                    {                        
                        return RedirectToAction(nameof(Index));
                    }
                    else if(User.IsInRole("Opérateur"))
                    {                       
                        return RedirectToAction(nameof(IndexUser));
                    }
                }
                catch
                {
                    //Si erreur lors de la mise à jour de la base de données message d'erreur
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                }
            }           
            
            return View(nonConformite);
        }


        //Méthode pour récupérer les informations de la nonconformité sélectionnée par l'id et qui permet de l'afficher
        // GET: NonConformites/Edit/5
        [HttpGet]
        [Authorize(Roles = "Opérateur,Qualité")]
        public async Task<IActionResult> Edit(int? id)
        {
            //Gestion de l'erreur si l'id est vide
            if (id == null)
            {
                return NotFound();
            }

            //récupération des informations de la non conformité en passant par le modèle NonConformité et sélectionné par l'id
            var nonConformite = await _context.NonConformite.FindAsync(id);

            //Gestion du résultat vide de la requête nonConformite
            if (nonConformite == null)
            {
                return NotFound();
           }

            //ViewData permettant d'avoir une liste déroulante de choix de produit dans la vue et d'enregistrer le choix sélectionné
            ViewData["ProduitId"] = new SelectList(_context.Set<Produit>(), "ProduitId", "Reference", nonConformite.ProduitId);
            return View(nonConformite);
        }

        //Méthode pour mettre à jour la nonconformité sélectionne dans le modèle NonConformite
        // POST: NonConformites/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Opérateur,Qualité")]
        public async Task<IActionResult> Edit(int id, [Bind("NonConformiteId,NumeroNC,NumeroNCIncrement,DescriptionNC,PhotoNC,DateNC,QuantiteProdNC,NumeroSerie,ProduitId,ApplicationAuthUserId,EtatActionId")] NonConformite nonConformite)
        {
            //Gestion de l'erreur si l'id est vide
            if (id != nonConformite.NonConformiteId)
            {
                return NotFound();
            }

            //Génération de la date du moment pour la création des NC           
            nonConformite.DateNC = DateTime.Now;
            //Génération du nom d'utilisateur connecté
            nonConformite.ApplicationAuthUserId = _userManager.GetUserId(User);

            //Génération de l'état en cours par défaut avec requête linq dans bdd etat action 
            nonConformite.EtatActionId = (ReqFullEtatNcByEtatAction().ToArray())[0];

            //ViewData permettant d'avoir une liste déroulante de choix de produit dans la vue et d'enregistrer le choix sélectionné
            ViewData["ProduitId"] = new SelectList(_context.Set<Produit>(), "ProduitId", "Reference", nonConformite.ProduitId);

            //Gestion de la photoNC si pas de nouveau téléchargement garder l'ancienne image
            var files = HttpContext.Request.Form.Files;

            if (0 != files.Count)
            {
                foreach (var PhotoNC in files)
                {
                    if (PhotoNC != null && PhotoNC.Length > 0)
                    {
                        var file = PhotoNC;

                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create);
                            await file.CopyToAsync(fileStream);
                            nonConformite.PhotoNC = fileName;
                        }
                    }
                }
            }
            else
            {
                //Recherche PhotoNC actuelle
                var PhotoActuelle = ReqPhotoByIdByNC(id).ToArray();
                nonConformite.PhotoNC = PhotoActuelle[0];
            }

            //Si le formulaire est valide
            if (ModelState.IsValid)
            {
                try
                {
                    //essai de mise à jour la base de données
                    _context.Update(nonConformite);
                    await _context.SaveChangesAsync();

                    //Redirection suivant le rôle de l'utilisateur connecté
                    if (User.IsInRole("Qualité"))
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else if (User.IsInRole("Opérateur"))
                    {
                        return RedirectToAction(nameof(IndexUser));
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    //Gestion de l'erreur si la NC n'existe pas
                    if (!NonConformiteExists(nonConformite.NonConformiteId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                    }
                   
                }

            }       
           
            
            return View(nonConformite);
        }


        //Méthode de récupération des informations de la NC en fonction de l'id pour affichage
        // GET: NonConformites/Delete/5
        [HttpGet]
        [Authorize(Roles = "Opérateur,Qualité")]
        public async Task<IActionResult> Delete(int? id)
        {
            //gestion de l'erreur si Id Inconnu
            if (id == null)
            {
                return NotFound();
            }

            //Récupération de l'information sur la NC en intégrant ApllicationAuthUser, EtatAction et Produit
            var nonConformite = await _context.NonConformite
                .Include(n => n.ApplicationAuthUser)
                .Include(n => n.EtatAction)
                .Include(n => n.Produit)
                .FirstOrDefaultAsync(m => m.NonConformiteId == id);

            //Si la requête n'a aucun résultat
            if (nonConformite == null)
            {
                return NotFound();
            }

            return View(nonConformite);
        }

        //Méthode de supression d'une nonconformité par rapport à l'ID
        // POST: NonConformites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Opérateur,Qualité")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //récupération de la nonconformité en fonction de l'id
            var nonConformite = await _context.NonConformite.FindAsync(id);

            //La  nonconformité peut être supprimer seulement si l'état est à traiter
            if(nonConformite.EtatActionId == "A traiter")
            {
                
                //Essai de suppression
                try
                 {
                _context.NonConformite.Remove(nonConformite);
                await _context.SaveChangesAsync();
                }
                 catch
                {
                    //Gestion d'un problème d'enregistrement dans la base de donnée
                ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                }

                //Redirection différente suivant le rôle de l'utilisateur connecté
                if (User.IsInRole("Qualité"))
                {                    
                    return RedirectToAction(nameof(Index));
                }
                else if (User.IsInRole("Opérateur"))
                {                    
                    return RedirectToAction(nameof(IndexUser));
                }
            }
            else
            {
                //Gestion de l'erreur si l'état est autre que à traiter
                ModelState.AddModelError(string.Empty, "Impossible de supprimer cette Non-conformité");
            }

            return View(nonConformite);

        }

        




    }
}
