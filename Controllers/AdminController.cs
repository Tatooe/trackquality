﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Data;
using TrackQuality.ViewModels;

//Classe permettant d'attribuer les rôles aux utilisateurs et de supprimer un utilisateur accessible seulement au rôle admin
namespace TrackQuality.Controllers
{
    public class AdminController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationAuthUser> _userManager;
        private readonly ApplicationContext _context;
        

        public AdminController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationAuthUser> userManager, ApplicationContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }

 
        //DEBUT SECTION GESTION ROLES POUR LES UTILISATEURS

        //GET: Admin/ListPers  liste des personnes avec leur rôles attitrés
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ListPers()
        {

            var users = await _userManager.Users.ToListAsync();
            var listPers = new List<IndexUserRoleModel>();
            foreach(ApplicationAuthUser user in users)
            {
                var model = new IndexUserRoleModel();
                model.Id = user.Id;
                model.Nom = user.Nom;
                model.Prenom = user.Prenom;
                model.Email = user.Email;
                model.Role = string.Join(",", _userManager.GetRolesAsync(user).Result.ToArray());
                listPers.Add(model);
            }

            ViewData["Role"] = ((from item in _roleManager.Roles select item.Name).ToArray());

            return View(listPers);

        }


        //Get: Admin/EditRolePers  Modification des rôles pour les utilisateurs
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditRolePers(string Id)
        {
            var users = await _context.Users.Where(x => x.Id == Id).SingleOrDefaultAsync();
            UserRoleModel userRole = new UserRoleModel();
            var userInRole = _context.UserRoles.Where(x => x.UserId == Id).Select(x => x.RoleId).ToList();
            userRole.applicationUser = users;

            userRole.ApplicationRoles = await _roleManager.Roles.Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id,
                Selected = userInRole.Contains(x.Id)
            }).ToListAsync();

            

            return View(userRole);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditRolePers(UserRoleModel model)
        {
            var applicationUser = await _userManager.FindByIdAsync(model.applicationUser.Id);
            var selectedRoleId = model.ApplicationRoles.Where(x => x.Selected).Select(x => x.Value);
            var AlreadyExist = _context.UserRoles.Where(x => x.UserId == model.applicationUser.Id).Select(x => x.RoleId).ToList();
            var toAdd = selectedRoleId.Except(AlreadyExist);
            var toRemove = AlreadyExist.Except(selectedRoleId);

            foreach (var item in toRemove)
            {
                try
                {
                    _context.UserRoles.Remove(new IdentityUserRole<string>
                    {
                        RoleId = item,
                        UserId = model.applicationUser.Id
                    });
                    _context.SaveChanges();                    
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue merci d'essayer plus tard ou de contacter votre administrateur.");
                }                
            }

            foreach(var item in toAdd)
            {
                try
                {
                    _context.UserRoles.Add(new IdentityUserRole<string>
                    {
                        RoleId = item,
                        UserId = model.applicationUser.Id
                    });

                    _context.SaveChanges();
                    
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue merci d'essayer plus tard ou de contacter votre administrateur.");
                }              
            
            }
            return RedirectToAction(nameof(ListPers));
        }


        //GET:AdminController/Delete Supression d'une personne sauf si elle a un rôle admin
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeletePers(string Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(Id);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Produits/Delete/5
        [HttpPost, ActionName("DeletePers")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeletePersConfirmed(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            //récupération de la liste des admin 
            var listRole = await _userManager.GetUsersInRoleAsync("Admin");
            var roleUser = await _userManager.GetRolesAsync(user);

            if (user == null)
            {
                return NotFound();
            }

            //Condition si il y a un seul utilisateur en mode admin, interdiction de le supprimer
            if (listRole.Count == 1 && roleUser.FirstOrDefault() == "Admin")
            {
                ModelState.AddModelError(string.Empty, "Vous ne pouvez pas supprimer cet admin avant d'en redéfinir un autre.");
                return View(user);
            }
            else
            {
                try
                {
                    await _userManager.DeleteAsync(user);
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue merci d'essayer plus tard ou de contacter votre administrateur.");
                }
            }



            return RedirectToAction(nameof(ListPers));
        }
  

    }
}



