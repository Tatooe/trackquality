﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Data;
using TrackQuality.Models;

namespace TrackQuality.Controllers
{
    public class ProduitsController : Controller
    {
        private readonly ApplicationContext _context;

        public ProduitsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: Liste des produits
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Produit.ToListAsync());
        }

       
        // GET: Produits/Création d'un nouveau produit
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Produits/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("ProduitId,Reference,Prix")] Produit produit)
        {

            //Requête pour vérifier si le produit existe déjà et éviter les doublons dans la BDD
            var produitExist = await _context.Produit.FirstOrDefaultAsync(s => s.Reference == produit.Reference);


            if (ModelState.IsValid)
            {
                if (produitExist == null)
                {
                    try {
                        _context.Add(produit);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    } catch
                    {
                        ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                    }

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Ce Produit existe déjà");
                }
            }

            return View(produit);


        }

        // GET: Produits/Edit/5
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var produit = await _context.Produit.FindAsync(id);
            if (produit == null)
            {
                return NotFound();
            }
            return View(produit);
        }

        // POST: Produits/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("ProduitId,Reference,Prix")] Produit produit)
        {
            if (id != produit.ProduitId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(produit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProduitExists(produit.ProduitId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(produit);
        }

        // GET: Produits/Delete/5
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var produit = await _context.Produit
                .FirstOrDefaultAsync(m => m.ProduitId == id);
            if (produit == null)
            {
                return NotFound();
            }

            return View(produit);
        }

        // POST: Produits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var produit = await _context.Produit.FindAsync(id);
            try
            {
                _context.Produit.Remove(produit);
                await _context.SaveChangesAsync();
               
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
            }
            return RedirectToAction(nameof(Index));
        }

        private bool ProduitExists(int id)
        {
            return _context.Produit.Any(e => e.ProduitId == id);
        }
    }
}
