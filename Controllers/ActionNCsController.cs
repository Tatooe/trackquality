﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TrackQuality.Areas.Identity.Data;
using TrackQuality.Data;
using TrackQuality.Models;
using TrackQuality.ViewModels;

//Classe de gestion des non conformités (create, read, update, delete)
namespace TrackQuality.Controllers
{
    public class ActionNCsController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly UserManager<ApplicationAuthUser> _userManager;
        public ActionNCsController(ApplicationContext context, UserManager<ApplicationAuthUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        private IQueryable<int> ReqNCIdByAnalyseIdFromAnalyseId(int? id) => from item in _context.AnalyseNC where item.AnalyseId == id select item.NonConformiteId;
        private IQueryable<NonConformite> ReqFullNCByNCIdFromNC(int? id) => from item in _context.NonConformite where item.NonConformiteId == id select item;
        private IQueryable<AnalyseNC> ReqFullAnalyseByAnalyseIdFromAnalyse(int? id) => from item in _context.AnalyseNC where item.AnalyseId == id select item;
        private IQueryable<String> ReqFullEtatNcByEtatAction() => from item in _context.EtatAction select item.Etat;
        //Requête pour récupérer les informations de la personne ayant ouvert la NC
        private IQueryable<ApplicationAuthUser> ReqPersCreateNC(string userId) => from item in _userManager.Users where item.Id == userId select item;
        //Méthode de création de paramètre de vue pour les nonconformites et l'analyse en fonction de l'id analyse
        private void NcAndAnalyseViewByIdAnalyse(int? id)
        {
            //récupération de l'id de la non conformité en passant par analyseNC
            var nonconformiteId = ReqNCIdByAnalyseIdFromAnalyseId(id).FirstOrDefault();
            //récupération de toutes les informations de la non conformité concernée
            var nonConformites = ReqFullNCByNCIdFromNC(nonconformiteId).FirstOrDefault();
            //Selection des items voulus de la non conformité pour affichage dans la vue

            ViewData["NumeroNC"] = nonConformites.NumeroNC;
            ViewData["NumeroSerie"] = nonConformites.NumeroSerie;
            ViewData["Quantite"] = nonConformites.QuantiteProdNC;
            ViewData["DescriptionNC"] = nonConformites.DescriptionNC;
            ViewData["Photo"] = nonConformites.PhotoNC;
            ViewData["Nom"] = ReqPersCreateNC(nonConformites.ApplicationAuthUserId).FirstOrDefault().Nom;
            ViewData["Prenom"] = ReqPersCreateNC(nonConformites.ApplicationAuthUserId).FirstOrDefault().Prenom;
            ViewData["DateNC"] = nonConformites.DateNC;

            //Récupération information de l'analyse par l'id analyse récupéré

            var analyse = (from item in _context.AnalyseNC where item.AnalyseId == id select item).FirstOrDefault();
            ViewData["MainDOeuvre"] = analyse.MainDOeuvre;
            ViewData["Methode"] = analyse.Methode;
            ViewData["Matiere"] = analyse.Matiere;
            ViewData["Milieu"] = analyse.Milieu;
            ViewData["Materiel"] = analyse.Materiel;
            ViewData["Imputation"] = analyse.ImputationId;
        }



        // GET: ActionNCs/Details/5
        [HttpGet]
        [Authorize(Roles = "Qualité, Opérateur")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                RedirectToAction("NotFound");
            }



            AnalyseActionByNCViewModel model = new AnalyseActionByNCViewModel
            {
                ActionNC = await _context.ActionNC.Include(a => a.analyseNC).FirstOrDefaultAsync(m => m.ActionId == id)
            };

            if (model.ActionNC == null)
            {
                RedirectToAction("NotFound");
            }

            var idAnalyse = _context.ActionNC.Where(p => p.ActionId == id).Select(p => p.analyseNCId).FirstOrDefault();

            model.AnalyseNC = await _context.AnalyseNC.Where(p => p.AnalyseId == idAnalyse).FirstOrDefaultAsync();

            var idNC = _context.AnalyseNC.Where(p => p.AnalyseId == idAnalyse).Select(p => p.NonConformiteId).FirstOrDefault();

            model.NonConformite = await _context.NonConformite.Include(p=>p.Produit).Include(p => p.ApplicationAuthUser).Where(p => p.NonConformiteId == idNC).FirstOrDefaultAsync();

            return View(model);
        }

        // GET: ActionNCs/Create
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NcAndAnalyseViewByIdAnalyse(id);

            return View();
        }

        // POST: ActionNCs/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Create(int id, [Bind("ActionId,DescriptionAction,DateAction,analyseNCId,Cloture,Rebuter")] ActionNC actionNC)
        {
            //Récupération des informations sur l'analyse pour récupérer l'id de la nonConformité associée
            var analyse = ReqFullAnalyseByAnalyseIdFromAnalyse(id).FirstOrDefault();
            var nonConformiteId = analyse.NonConformiteId;

            //Récupération du context de la nonConformite pour modifier l'état de la NonConformité
            var nonConformite = await _context.NonConformite.Where(x => x.NonConformiteId == nonConformiteId).SingleOrDefaultAsync();
                        
            //Récupération de la table état pour pouvoir changer l'état de la non conformité
            var etat = ReqFullEtatNcByEtatAction().ToArray();

            //Ajout par défaut de l'id de l'analyse
            actionNC.analyseNCId = id;

            //Ajout par défaut de la date de la validation de l'action
            actionNC.DateAction = DateTime.Now;

            if (ModelState.IsValid)
            {
                //modification de l'état de la NC à solder lors de la validation des actions
                nonConformite.EtatActionId = etat[2];
                try {
                    _context.Update(nonConformite);
                    await _context.SaveChangesAsync();
                } catch
                {
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                }
                try
                {
                    _context.Add(actionNC);
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, "Une erreur est survenue, merci d'essayer plus tard");
                }
                

                if(actionNC.Cloture == false)
                {
                    return RedirectToAction("Index", "TimeLine", new { id = nonConformiteId });
                }
                else
                {
                    return RedirectToAction("Index", "NonConformites");
                }
                
                
            }

            /**
             *Préparation de la vue 
             */
            //récupération de l'id de l'analyse
            var analyseId = analyse.AnalyseId;
            NcAndAnalyseViewByIdAnalyse(analyseId);

            return View(actionNC);
        }

        // GET: ActionNCs/Edit/5
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                RedirectToAction("NotFound");
            }

            AnalyseActionByNCViewModel model = new AnalyseActionByNCViewModel();
            
            model.ActionNC = await _context.ActionNC.Include(a => a.analyseNC).FirstOrDefaultAsync(m => m.ActionId == id);

            if (model.ActionNC == null)
            {
                RedirectToAction("NotFound");
            }

            var idAnalyse = _context.ActionNC.Where(p => p.ActionId == id).Select(p => p.analyseNCId).FirstOrDefault();

            model.AnalyseNC = await _context.AnalyseNC.Where(p => p.AnalyseId == idAnalyse).FirstOrDefaultAsync();

            var idNC = _context.AnalyseNC.Where(p => p.AnalyseId == idAnalyse).Select(p => p.NonConformiteId).FirstOrDefault();

            model.NonConformite = await _context.NonConformite.Include(p => p.Produit).Include(p => p.ApplicationAuthUser).Where(p => p.NonConformiteId == idNC).FirstOrDefaultAsync();

            return View(model);
        }

        // POST: ActionNCs/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Qualité")]
        public async Task<IActionResult> Edit(int id, AnalyseActionByNCViewModel model)
        {

            if (id != model.ActionNC.ActionId)
            {
                return NotFound();
            }

           
            model.ActionNC.DateAction = DateTime.Now;

            if (model.NonConformite.QuantiteProdNC < model.NonConformite.QtRebute)
            {
                ModelState.AddModelError(string.Empty, "La quantité rebutée ne peut pas être supérieur à la quantité de produit non-conforme.");
            }


            if (ModelState.IsValid)
            {


                try
                {
                    _context.Update(new NonConformite
                    {
                        NonConformiteId = model.NonConformite.NonConformiteId,
                        NumeroNC=model.NonConformite.NumeroNC,
                        DescriptionNC = model.NonConformite.DescriptionNC,
                        DateNC = model.NonConformite.DateNC,
                        QuantiteProdNC =model.NonConformite.QuantiteProdNC,
                        NumeroSerie = model.NonConformite.NumeroSerie,
                        ProduitId = model.NonConformite.ProduitId,
                        ApplicationAuthUserId = model.NonConformite.ApplicationAuthUserId,
                        EtatActionId = model.NonConformite.EtatActionId,
                        PhotoNC = model.NonConformite.PhotoNC,
                        QtRebute = model.NonConformite.QtRebute
                    });

                    _context.Update(
                       new ActionNC
                       {
                           ActionId = model.ActionNC.ActionId,
                           DescriptionAction = model.ActionNC.DescriptionAction,
                           DateAction = model.ActionNC.DateAction,
                           analyseNCId = model.ActionNC.analyseNCId,
                           Cloture = model.ActionNC.Cloture
                       });
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActionNCExists(model.ActionNC.ActionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                if (model.ActionNC.Cloture == false)
                {
                    return RedirectToAction("Index", "TimeLine", new { id = model.NonConformite.NonConformiteId });
                }
                else
                {
                    return RedirectToAction("Index", "NonConformites");
                }
            }

            return View(model);
        }


        private bool ActionNCExists(int id)
        {
            return _context.ActionNC.Any(e => e.ActionId == id);
        }
    }
}
