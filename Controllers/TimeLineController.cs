﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackQuality.Data;

namespace TrackQuality.Controllers
{
    public class TimeLineController : Controller
    {
        private readonly ApplicationContext _context;

        public TimeLineController(ApplicationContext context)
        {
            _context = context;
        }
        //Vue sur les étapes du traitement à faire
        // GET: AnalyseNCs
        [HttpGet]
        [Authorize(Roles = "Qualité")]
        public IActionResult Index(int? id)
        {

            var nonConformites = (from item in _context.NonConformite where item.NonConformiteId == id select item).ToArray();
            ViewData["Etat"] = nonConformites[0].EtatActionId;
            ViewData["NonConformiteId"] = nonConformites[0].NonConformiteId;
            ViewData["DateNC"] = nonConformites[0].DateNC;
            ViewData["NumeroNC"] = nonConformites[0].NumeroNC;

            //Récupérationdes informations de l'analyse en cas d'état en cours
            if (nonConformites[0].EtatActionId == "En Cours")
            {
                var analyse = (from item in _context.AnalyseNC where item.NonConformiteId == id select item).ToArray();
                ViewData["DateAnalyse"] = analyse[0].DateAnalyse;
                ViewData["AnalyseId"] = analyse[0].AnalyseId;
                ViewData["NonConformiteId"] = analyse[0].NonConformiteId;

                var nonConformite = (from item in _context.NonConformite where item.NonConformiteId == analyse[0].NonConformiteId select item).ToArray();
                ViewData["DateNC"] = nonConformite[0].DateNC;
                ViewData["NumeroNC"] = nonConformite[0].NumeroNC;
                ViewData["Etat"] = nonConformite[0].EtatActionId;
                ViewData["NonConformiteId"] = nonConformite[0].NonConformiteId;
            }

            if (nonConformites[0].EtatActionId == "Fermer")
            {
                var analyse = (from item in _context.AnalyseNC where item.NonConformiteId == id select item).ToArray();
                ViewData["DateAnalyse"] = analyse[0].DateAnalyse;
                ViewData["AnalyseId"] = analyse[0].AnalyseId;

                var action = (from item in _context.ActionNC where item.analyseNCId == analyse[0].AnalyseId select item).ToArray();
                ViewData["DateAction"] = action[0].DateAction;
                ViewData["ActionId"] = action[0].ActionId;
            }
            return View();
        }
    }
}
